<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DhaFees;
use DB;

class DHAController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dhaFees = DhaFees::all();
        $data=DB::table("dha_fees")->sum('first_payment');
        return view('officio.dhafees.index',compact('dhaFees','data'))->with('title','DHA/Fees');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\ResponseC
     */
    public function create()
    {
        return view('officio.dhafees.create')->with('title','Create DHA/Fees');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
           'name' => 'required',
            'qualification' => 'required',
            'applied_for' => 'required',
            'first_payment' => 'required',
           
        ]);
        $input = $request->all();
        if ($input) {
            DhaFees::create($input);
            session()->flash('message', 'DHA/Fees Created.');
            return redirect('admin/DHA');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dhaFee = DhaFees::find($id);
        return view('officio.dhafees.edit',compact('dhaFee'))->with('title','Edit DHA/Fee');
    }

    /**
     * Update the specified rescashOutource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'qualification' => 'required',
            'applied_for' => 'required',
            'first_payment' => 'required',
        ]);
        $dhaFee = DhaFees::find($id);
        $input = $request->all();
        $dhaFee->update($input);
        session()->flash('message', 'DHA/Fees Updated.');
        return redirect('admin/DHA');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!request()->ajax()){
            return false;
        }

        DhaFee::find($id)->delete();


        session()->flash('message', 'DHA/Fees Deleted.');

        return response()->json(array(
            'status' => 'success',
        ));
    }
}

