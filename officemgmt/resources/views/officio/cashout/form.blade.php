<div class="box-body">
    <div class="form-group" {{ $errors->has('expenses_type') ? ' has-error' : '' }}>
        <label for="expenses_type" class="col-sm-2 control-label">expenses_type:<span class=help-block"
            style="color: #b30000">&nbsp;* </span></label>

            <div class="col-sm-8">

    <select name="expenses_type" class="form-control">
                                        <option value="Rent">Rent</option>
                                        <option value="Electricity & Garbage">Electricity & Garbage</option>
                                        <option value="Internet">Internet</option>
                                        <option value="Book Printing">Book Printing</option>
                                        <option value="Advertising & Marketing">Advertising & Marketing</option>
                                        <option value="Cancellation/Refund">Cancellation/Refund</option>
                                        <option value="Telephone">Telephone</option>
                                        <option value="Stationery">Stationery</option>
                                        <option value="Lunch">Lunch</option>
                                        <option value="Snacks & tea">Snacks & tea</option>
                                        <option value="Staff Salary">Staff Salary</option>
                                        <option value="Fuel & travel expenses">Fuel & travel expenses</option>
                                        <option value="Repair & maintenance">Repair & maintenance</option>
                                        <option value="Loan Payment">Loan Payment</option>
                                        <option value="Basic Life Support">Basic Life Support</option>
                                        <option value="Others">Others</option>
                                        
                                    </select>
                @if ($errors->has('expenses_type'))
                <span class="help-block">
                    <strong> {{ $errors->first('expenses_type') }}</strong>
                </span>
                @endif

            </div>
        </div>

        <div class="form-group" {{ $errors->has('date') ? ' has-error' : '' }}>
            <label for="date" class="col-sm-2 control-label">date:<span class=help-block"
                style="color: #b30000">&nbsp;* </span></label>

                <div class="col-sm-8">
                    {!! Form::date('date', null , ['class'=> 'form-control', 'placeholder' => ' date', 'id'=>"date"]) !!}

                    @if ($errors->has('date'))
                    <span class="help-block">
                        <strong> {{ $errors->first('date') }}</strong>
                    </span>
                    @endif

                </div>
            </div>
            <div class="form-group" {{ $errors->has('amount') ? ' has-error' : '' }}>
                <label for="amount" class="col-sm-2 control-label">amount:<span class=help-block"
                    style="color: #b30000">&nbsp;* </span></label>

                    <div class="col-sm-8">
                        {!! Form::text('amount', null , ['class'=> 'form-control', 'placeholder' => ' amount', 'id'=>"amount"]) !!}

                        @if ($errors->has('amount'))
                        <span class="help-block">
                            <strong> {{ $errors->first('amount') }}</strong>
                        </span>
                        @endif

                    </div>
                </div>
                
                <div class="form-group" {{ $errors->has('paid_by') ? ' has-error' : '' }}>
                    <label for="paid_by" class="col-sm-2 control-label">paid_by:<span class=help-block"
                        style="color: #b30000">&nbsp;* </span></label>

                        <div class="col-sm-8">
                            {!! Form::text('paid_by', null , ['class'=> 'form-control', 'placeholder' => ' paid_by', 'id'=>"paid_by"]) !!}

                            @if ($errors->has('paid_by'))
                            <span class="help-block">
                                <strong> {{ $errors->first('paid_by') }}</strong>
                            </span>
                            @endif

                        </div>
                    </div>
                    
                    <div class="form-group" {{ $errors->has('received_by') ? ' has-error' : '' }}>
                        <label for="received_by" class="col-sm-2 control-label">received_by:<span class=help-block"
                            style="color: #b30000">&nbsp;* </span></label>

                            <div class="col-sm-8">
                                {!! Form::text('received_by', null , ['class'=> 'form-control', 'placeholder' => ' received_by', 'id'=>"received_by"]) !!}

                                @if ($errors->has('received_by'))
                                <span class="help-block">
                                    <strong> {{ $errors->first('received_by') }}</strong>
                                </span>
                                @endif

                            </div>
                        </div>
                        
                        <div class="form-group" {{ $errors->has('payment_mode') ? ' has-error' : '' }}>
                            <label for="payment_mode" class="col-sm-2 control-label">payment_mode:<span class=help-block"
                                style="color: #b30000">&nbsp;* </span></label>

                                <div class="col-sm-8">
                                 <!--  {!! Form::select('payment_mode' ,['c'=>'cash','ch'=>'cheque','b'=>'bank'],null , ['class'=> 'form-control', 'placeholder' => 'payment_mode', 'id'=>"payment_mode"]) !!} -->
    <select name="payment_mode" class="form-control">
                                        <option value="cash">cash</option>
                                        <option value="cheque">cheque</option>
                                        <option value="bank">bank</option>
                                    </select>
                                  @if ($errors->has('payment_mode'))
                                  <span class="help-block">
                                    <strong> {{ $errors->first('payment_mode') }}</strong>
                                </span>
                                @endif

                            </div>
                        </div>
                        
                        

                        @if(Request::segment(4) == 'edit')
                        <div class="form-group">
                            <label for="status" class="col-sm-2 control-label">Status<span class=help-block" style="color: #b30000">&nbsp;* </span></label>

                            <div class="col-sm-8">

                                <label class="radio-inline"><input type="radio" id="active" name="status" value=1
                                 @if($cashOut->status==1) checked @endif >Active</label>&nbsp;
                                 <label class="radio-inline"><input type="radio" id="inactive" name="status" value=0
                                     @if($cashOut->status==0) checked @endif >Inactive</label>
                                 </div>
                             </div>
                             @endif
                         </div>
