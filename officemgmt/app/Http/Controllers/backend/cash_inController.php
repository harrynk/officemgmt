<?php
namespace App\Http\Controllers\backend;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Cash_in;
use DB;
use App\Report;
use Validator;
class cash_inController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cashIn = Cash_in::all();
        $data = DB::table("cash_ins")->sum('service_charge');


        return view('officio.cashin.index',compact('cashIn','data'))->with('title','Cash in');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('officio.cashin.create')->with('title','Create Cash in');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $this->validate($request, [
            'service_type' => 'required',
            'service_charge' => 'required',


            // 'date' => 'required',

            'paid_by' => 'required',
            'received_by' => 'required',



        ]);
        $input = $request->all();
        $cashins=Cash_in::pluck('service_charge','id');
        if ($input) {

         $cash_entered=Cash_in::create($input);
         

            session()->flash('message', 'Cash in Created.');
            return redirect('admin/cash_in');
        }
          $reports= Report::create([
                    'cashin_id' =>$ca,
                    // 'client_id'=>$clients->id,
                    // 'action'=>'New Client'.' '.$request->name.' '.'Created.',
                ]);
    }


//     public function store(Request $request){

//         $input = $request->all();
//         $rules = [

//         'service_type' => 'required',
//         'service_charge' => 'required',


//         'date' => 'required',
//         'payment_mode' => 'required',

//         'paid_by' => 'required',
//         'received_by' => 'required',
//         ];

//         $validate = Validator::make($request->all(),$rules);

//         if($validate->fails()){

//             return redirect('admin/cash_in')
//             ->withErrors($validate)
//             ->withInput();
//         }
//         else{
//             // $input['password'] = $request->password ? bcrypt($request->password):bcrypt('default');
//             // $users = User::create($input);
            
//             // $input['user_id'] = $users->id;
//             $cashins = Cash_in::create($input);
//             if($cashins){

//                 // if($request->segment(1)=='admin'){
//                 // // if(Auth::guard('admin')){
//                 //     $staff_id = Null;
//                 // }
//                 // else{
//                 //     $staff_id = Auth::guard('employee')->user()->id;
//                 // }
//                 $track_record = Report::create([
                   
//                     'cashin_id'=>$cashins->service_charge,
                  
//                     ]);
//             }

//             Session()->flash('message','Cash In Added Successfully.');
//             // if(Auth::guard('admin')->user()){
        
//     }
// }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cashIn = Cash_in::find($id);
        return view('officio.cashin.edit',compact('cashIn'))->with('title','Edit Visa Type');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
           'service_type' => 'required',
           'date' => 'required',
           'service_charge' => 'required',
           'paid_by' => 'required',
           'received_by' => 'required',
           'payment_mode' => 'required',



           ]);
        $cashIn = Cash_in::find($id);
        $input = $request->all();
        $cashIn->update($input);
        session()->flash('message', 'Cash In Updated.');
        return redirect('admin/cash_in');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!request()->ajax()){
            return false;
        }

        Cash_in::find($id)->delete();


        session()->flash('message', 'Cash In Deleted.');

        return response()->json(array(
            'status' => 'success',
            ));
    }
}
