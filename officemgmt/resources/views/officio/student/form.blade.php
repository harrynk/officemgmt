<div class="box-body">

    
    <div class="form-group" {{ $errors->has('name') ? ' has-error' : '' }}>
        <label for="name" class="col-sm-2 control-label">Student Name:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">
          {!! Form::text('name', null , ['class'=> 'form-control', 'placeholder' => ' name', 'id'=>"name"]) !!}

            @if ($errors->has('name'))
                <span class="help-block">
                    <strong> {{ $errors->first('name') }}</strong>
                </span>
            @endif

        </div>
    </div>

        <div class="form-group" {{ $errors->has('ph_no') ? ' has-error' : '' }}>
            <label for="ph_no" class="col-sm-2 control-label">Phone No:<span class=help-block"
                style="color: #b30000">&nbsp;* </span></label>

                <div class="col-sm-8">
                    {!! Form::text('ph_no', null , ['class'=> 'form-control', 'placeholder' => ' ph_no', 'id'=>"ph_no"]) !!}

                    @if ($errors->has('ph_no'))
                    <span class="help-block">
                        <strong> {{ $errors->first('ph_no') }}</strong>
                    </span>
                    @endif

                </div>
            </div>
  <div class="form-group" {{ $errors->has('mb_no') ? ' has-error' : '' }}>
            <label for="mb_no" class="col-sm-2 control-label">Mobile No:<span class=help-block"
                style="color: #b30000">&nbsp;* </span></label>

                <div class="col-sm-8">
                    {!! Form::text('mb_no', null , ['class'=> 'form-control', 'placeholder' => ' mb_no', 'id'=>"mb_no"]) !!}

                    @if ($errors->has('mb_no'))
                    <span class="help-block">
                        <strong> {{ $errors->first('mb_no') }}</strong>
                    </span>
                    @endif

                </div>
            </div>
       <div class="form-group" {{ $errors->has('address') ? ' has-error' : '' }}>
        <label for="address" class="col-sm-2 control-label">Address:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">
          {!! Form::text('address', null , ['class'=> 'form-control', 'placeholder' => ' address', 'id'=>"address"]) !!}

            @if ($errors->has('address'))
                <span class="help-block">
                    <strong> {{ $errors->first('address') }}</strong>
                </span>
            @endif

        </div>
    </div>
    <div class="form-group" {{ $errors->has('email') ? ' has-error' : '' }}>
        <label for="email" class="col-sm-2 control-label">Email:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">
          {!! Form::text('email', null , ['class'=> 'form-control', 'placeholder' => ' email', 'id'=>"email"]) !!}

            @if ($errors->has('email'))
                <span class="help-block">
                    <strong> {{ $errors->first('email') }}</strong>
                </span>
            @endif

        </div>
    </div>
    <div class="form-group" {{ $errors->has('qualification') ? ' has-error' : '' }}>
        <label for="qualification" class="col-sm-2 qualification-label">Qualification:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">
      <select name="qualification" class="form-control" placeholder="qualification" id="qualification">
                                        <option value="School Leaving Certificate">School Leaving Certificate</option>
                                        <option value="+2/A-level">+2/A-level</option>
                                        <option value="Diploma">Diploma</option>
                                        <option value="Bachelors">Bachelors</option>
                                        <option value="Master">Master</option>
                                       
                                      
                                    </select>

            @if ($errors->has('qualification'))
                <span class="help-block">
                    <strong> {{ $errors->first('qualification') }}</strong>
                </span>
            @endif

        </div>
    </div>
    <div class="form-group" {{ $errors->has('subject') ? ' has-error' : '' }}>
        <label for="subject" class="col-sm-2 control-label">Subject:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">
           <select name="subject" class="form-control" placeholder="subject" id="subject">
                                        <option value="Science">Science</option>
                                        <option value="Commerce">Commerce</option>
                                        <option value="Art">Art</option>
                                        <option value="Education">Education</option>
                                   
                                       
                                      
                                    </select>

            @if ($errors->has('subject'))
                <span class="help-block">
                    <strong> {{ $errors->first('subject') }}</strong>
                </span>
            @endif

        </div>
    </div>
    <div class="form-group" {{ $errors->has('title') ? ' has-error' : '' }}>
        <label for="title" class="col-sm-2 control-label">Title of qualification:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">
          {!! Form::text('title', null , ['class'=> 'form-control', 'placeholder' => ' title', 'id'=>"title"]) !!}

            @if ($errors->has('title'))
                <span class="help-block">
                    <strong> {{ $errors->first('title') }}</strong>
                </span>
            @endif

        </div>
    </div>
    <div class="form-group" {{ $errors->has('experience') ? ' has-error' : '' }}>
        <label for="experience" class="col-sm-2 control-label">Years of experience:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">
        <!--   {!! Form::text('experience', null , ['class'=> 'form-control', 'placeholder' => ' experience', 'id'=>"experience"]) !!} -->
  <select name="experience" class="form-control" placeholder="experience" id="experience">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10 or more">10 or more</option>
                                      
                                    </select>
            @if ($errors->has('experience'))
                <span class="help-block">
                    <strong> {{ $errors->first('experience') }}</strong>
                </span>
            @endif

        </div>
    </div>
    <div class="form-group" {{ $errors->has('work_sector') ? ' has-error' : '' }}>
        <label for="work_sector" class="col-sm-2 control-label">Work sector:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">
          {!! Form::text('work_sector', null , ['class'=> 'form-control', 'placeholder' => ' work_sector', 'id'=>"work_sector"]) !!}

            @if ($errors->has('work_sector'))
                <span class="help-block">
                    <strong> {{ $errors->first('work_sector') }}</strong>
                </span>
            @endif

        </div>
    </div>
     <div class="form-group" {{ $errors->has('language_test') ? ' has-error' : '' }}>
        <label for="language_test" class="col-sm-2 control-label">language test:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">
          <select name="language_test" class="form-control" placeholder="language_test" id="language_test">
                                        <option value="CAE">CAE</option>
                                        <option value="IELTS">IELTS</option>
                                        <option value="TOEFL">TOEFL</option>
                                        <option value="PTE">PTE</option>
                                        <option value="SAT">SAT</option>
                                       
                                        <option value="GRE">GRE</option>
                                        <option value="GMAT">GMAT</option>
                                      
                                      
                                    </select>

            @if ($errors->has('language_test'))
                <span class="help-block">
                    <strong> {{ $errors->first('language_test') }}</strong>
                </span>
            @endif

        </div>
    </div>
 
    <div class="form-group" {{ $errors->has('test_score') ? ' has-error' : '' }}>
        <label for="test_score" class="col-sm-2 control-label">Language test score:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">
          {!! Form::text('test_score', null , ['class'=> 'form-control', 'placeholder' => ' test_score', 'id'=>"test_score"]) !!}

            @if ($errors->has('test_score'))
                <span class="help-block">
                    <strong> {{ $errors->first('test_score') }}</strong>
                </span>
            @endif

        </div>
    </div>
  
    <div class="form-group" {{ $errors->has('service') ? ' has-error' : '' }}>
        <label for="service" class="col-sm-2 control-label">Service if any:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">
       <select name="service" class="form-control" placeholder="service" id="service">
                                        <option value="Study Abroad">Study Abroad</option>
                                        <option value="Visa">Visa</option>
                                        <option value="Passport">Passport</option>
                                        <option value="Permanent Ressidency(PR)-Canada">Permanent Ressidency(PR)-Canada</option>
                                        <option value="English Translation">English Translation</option>
                                       
                                        <option value="Taxi Service">Taxi Service
                                    </select>

            @if ($errors->has('service'))
                <span class="help-block">
                    <strong> {{ $errors->first('service') }}</strong>
                </span>
            @endif

        </div>
    </div>
    
    @if(Request::segment(4) == 'edit')
        <div class="form-group">
            <label for="status" class="col-sm-2 control-label">Status<span class=help-block" style="color: #b30000">&nbsp;* </span></label>

            <div class="col-sm-8">

                <label class="radio-inline"><input type="radio" id="active" name="status" value=1
                                                   @if($student->status==1) checked @endif >Active</label>&nbsp;
                <label class="radio-inline"><input type="radio" id="inactive" name="status" value=0
                                                   @if($student->status==0) checked @endif >Inactive</label>
            </div>
        </div>
    @endif
</div>
