<!-- {!! Form::hidden('dha_id', 2 , ['class'=> 'form-control', 'placeholder' => ' username', 'id'=>"username"]) !!} -->
  
    <div class="form-group" {{ $errors->has('dha_id') ? ' has-error' : '' }}>
        <label for="dha_id" class="col-sm-2 control-label">Preferred Date From:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">
          {!! Form::hidden('dha_id', null , ['class'=> 'form-control', 'placeholder' => ' dha_id', 'id'=>"dha_id"]) !!}

            @if ($errors->has('dha_id'))
                <span class="help-block">
                    <strong> {{ $errors->first('dha_id') }}</strong>
                </span>
            @endif

        </div>
    </div>
<div class="box-body">

    
    <div class="form-group" {{ $errors->has('preferred_date_from') ? ' has-error' : '' }}>
        <label for="preferred_date_from" class="col-sm-2 control-label">Preferred Date From:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">
          {!! Form::date('preferred_date_from', null , ['class'=> 'form-control', 'placeholder' => ' preferred_date_from', 'id'=>"preferred_date_from"]) !!}

            @if ($errors->has('preferred_date_from'))
                <span class="help-block">
                    <strong> {{ $errors->first('preferred_date_from') }}</strong>
                </span>
            @endif

        </div>
    </div>

        <div class="form-group" {{ $errors->has('preferred_date_to') ? ' has-error' : '' }}>
            <label for="preferred_date_to" class="col-sm-2 control-label">TO:<span class=help-block"
                style="color: #b30000">&nbsp;* </span></label>

                <div class="col-sm-8">
                    {!! Form::date('preferred_date_to', null , ['class'=> 'form-control', 'placeholder' => ' preferred_date_to', 'id'=>"preferred_date_to"]) !!}

                    @if ($errors->has('preferred_date_to'))
                    <span class="help-block">
                        <strong> {{ $errors->first('preferred_date_to') }}</strong>
                    </span>
                    @endif

                </div>
            </div>

        <div class="form-group" {{ $errors->has('dha/mcq') ? ' has-error' : '' }}>
            <label for="dha/mcq" class="col-sm-2 control-label">DHA/MCQ :<span class=help-block"
                style="color: #b30000">&nbsp;* </span></label>

                <div class="col-sm-8">
                   <!--  {!! Form::text('dha/mcq', null , ['class'=> 'form-control', 'placeholder' => ' dha/mcq', 'id'=>"dha/mcq"]) !!} -->
  <input type="radio"  name="dha/mcq" value="Pass"/> Pass &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <input type="radio"  name="dha/mcq" value="Fail"/> Fail
                    @if ($errors->has('dha/mcq'))
                    <span class="help-block">
                        <strong> {{ $errors->first('dha/mcq') }}</strong>
                    </span>
                    @endif

                </div>
            </div>


        <div class="form-group" {{ $errors->has('exam_date_booked') ? ' has-error' : '' }}>
            <label for="exam_date_booked" class="col-sm-2 control-label">Exam Date Booked:<span class=help-block"
                style="color: #b30000">&nbsp;* </span></label>

                <div class="col-sm-8">
                    {!! Form::date('exam_date_booked', null , ['class'=> 'form-control', 'placeholder' => ' exam_date_booked', 'id'=>"exam_date_booked"]) !!}

                    @if ($errors->has('exam_date_booked'))
                    <span class="help-block">
                        <strong> {{ $errors->first('exam_date_booked') }}</strong>
                    </span>
                    @endif

                </div>
            </div>

        <div class="form-group" {{ $errors->has('dha_status') ? ' has-error' : '' }}>
            <label for="dha_status" class="col-sm-2 control-label">DHA Status :<span class=help-block"
                style="color: #b30000">&nbsp;* </span></label>

                <div class="col-sm-8">
                 <!--    {!! Form::text('dha_status', null , ['class'=> 'form-control', 'placeholder' => ' dha_status', 'id'=>"dha_status"]) !!} -->
   <input type="checkbox" onclick="ShowHideDiv(this)" id="value"  name="dha_status" value="Pass"/> Pass &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <input type="checkbox"  id="checkbox1" class="dha_status" name="dha_status" value="Fail"/> Fail
                    @if ($errors->has('dha_status'))
                    <span class="help-block">
                        <strong> {{ $errors->first('dha_status') }}</strong>
                    </span>
                    @endif

                </div>
            </div>
      
        <div class="form-group" {{ $errors->has('eligibility') ? ' has-error' : '' }}>
            <label for="eligibility" class="col-sm-2 control-label">Eligibility:<span class=help-block"
                style="color: #b30000">&nbsp;* </span></label>

                <div class="col-sm-8">
                 <!--    {!! Form::text('eligibility', null , ['class'=> 'form-control', 'placeholder' => ' eligibility', 'id'=>"eligibility"]) !!} -->
   <input type="checkbox" id="eligibility" name="eligibility"  value="Yes"/> Yes &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <input type="checkbox" id="eligibility" name="eligibility" value="No"/> No
                    @if ($errors->has('eligibility'))
                    <span class="help-block">
                        <strong> {{ $errors->first('eligibility') }}</strong>
                    </span>
                    @endif

                </div>
            </div>
    @if(Request::segment(4) == 'edit')
        <div class="form-group">
            <label for="status" class="col-sm-2 control-label">Status<span class=help-block" style="color: #b30000">&nbsp;* </span></label>

            <div class="col-sm-8">

                <label class="radio-inline"><input type="radio" id="active" name="status" value=1
                                                   @if($examstatus->status==1) checked @endif >Active</label>&nbsp;
                <label class="radio-inline"><input type="radio" id="inactive" name="status" value=0
                                                   @if($examstatus->status==0) checked @endif >Inactive</label>
            </div>
        </div>
    @endif
</div>
