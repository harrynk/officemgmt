<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Reminder;


class ReminderController extends Controller
{
    /**nder
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reminders = Reminder::all();
     
        return view('officio.reminder.index',compact('reminders'))->with('title','Reminder');
    }
    /**Griffith 
     * Show the form for creating a new resource.
     *ResponseC
     * @return \Illuminate\Http\
     */
    public function create()
    {
        return view('officio.reminder.create')->with('title','Create Griffith reminder');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          
            'subject' => 'required',
            'message' => 'required',
           
           
        ]);
        $input = $request->all();
        if ($input) {
            Reminder::create($input);
            session()->flash('message', 'Griffith Reminder Created.');
            return redirect('admin/reminder');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reminder = Reminder::find($id);
        return view('officio.reminder.edit',compact('reminder'))->with('title','Edit Griffith Reminder');
    }

    /**
     * Update the specified rescashOutource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
           'date' => 'required',
            'subject' => 'required',
            'message' => 'required',
        
        ]);
        $reminder = Reminder::find($id);
        $input = $request->all();
        $reminder->update($input);
        session()->flash('message', 'Griffith Reminder Updated.');
        return redirect('admin/reminder');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!request()->ajax()){
            return false;
        }

       Reminder::find($id)->delete();


        session()->flash('message', 'Griffith Reminder Deleted.');

        return response()->json(array(
            'status' => 'success',
        ));
    }
}

