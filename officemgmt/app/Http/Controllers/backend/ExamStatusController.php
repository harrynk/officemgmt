<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DhaFees;
use App\ExamStatus;
use DB;
use App\User;
class ExamStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $examstatus = ExamStatus::all();
      
        $dhafee=DhaFee::all();
        // $dha_fees=DhaFees::pluck('name','id');
        return view('officio.status.index',compact('examstatus','dhafee'))->with('title','Exam  Status');
    }

    /**
     * Show the form for creating a new resource.e
     *
     * @return \Illuminate\Http\ResponseC
     */
    public function create()
    {
      $ExamStatus = ExamStatus::all();

      $dhaFees_detail=DhaFees::all();
      $dhafees=DhaFees::pluck('name','id');
      return view('officio.status.createdhastatus',compact('dhafees','dhaFees_detail','examstatus'))->with('title','Create Exam Status');
  }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
         'passport_no' => 'required',
         'dha_ref_no' => 'required',
         'barcode_no' => 'required',
         'remarks' => 'required',


         ]);
        $input = $request->all();

        if ($input) {
     

     
      
      
           ExamStatus::create($input);
           session()->flash('message', 'Exam Status  Created.');
           return redirect('admin/examstatus');
       }
   }
 
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $ExamStatus = CashOut::find($id);
     $examstatus=ExamStatus::find($id);
  
     $dhafees=DhaFees::all();  
     return view('officio.status.edit',compact('examstatus','dhafees'))->with('title','Edit Exam Status Status');
 }

    /**
     * Update the specified rescashOutource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
           'passport_no' => 'required',
         'dha_ref_no' => 'required',
         'barcode_no' => 'required',
         'remarks' => 'required',
            ]);
        $examstatus = ExamStatus::find($id);
        $input = $request->all();
        $examstatus->update($input);
        session()->flash('message', 'Exam Status Updated.');
        return redirect('admin/examstatus');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!request()->ajax()){
            return false;
        }

        ExamStatus::find($id)->delete();


        session()->flash('message', 'Exam Status Deleted.');

        return response()->json(array(
            'status' => 'success',
            ));
    }
}

