<div class="box-body">
    <div class="form-group" {{ $errors->has('name') ? ' has-error' : '' }}>
        <label for="name" class="col-sm-2 control-label">name:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">
          {!! Form::text('name', null , ['class'=> 'form-control', 'placeholder' => ' name', 'id'=>"name"]) !!}

            @if ($errors->has('name'))
                <span class="help-block">
                    <strong> {{ $errors->first('name') }}</strong>
                </span>
            @endif

        </div>
    </div>

       <div class="form-group" {{ $errors->has('qualification') ? ' has-error' : '' }}>
                        <label for="qualification" class="col-sm-2 control-label">qualification:<span class=help-block"
                            style="color: #b30000">&nbsp;* </span></label>

                            <div class="col-sm-8">
                                {!! Form::text('qualification', null , ['class'=> 'form-control', 'placeholder' => ' qualification', 'id'=>"qualification"]) !!}

                                @if ($errors->has('qualification'))
                                <span class="help-block">
                                    <strong> {{ $errors->first('qualification') }}</strong>
                                </span>
                                @endif

                            </div>
                        </div>
    <div class="form-group" {{ $errors->has('applied_for') ? ' has-error' : '' }}>
        <label for="applied_for" class="col-sm-2 control-label">applied_for:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">
          <!--   {!! Form::text('applied_for', null , ['class'=> 'form-control', 'placeholder' => ' applied_for', 'id'=>"applied_for"]) !!} -->
    <select name="applied_for" class="form-control">
                                        <option value="DHA-ARN">DHA-ARN</option>
                                        <option value="DHA-RN">DHA-RN</option>
                                        <option value="DHA-BMIT">DHA-BMIT</option>
                                        <option value="DHA-BPT">DHA-BPT</option>
                                        <option value="DHA-GP">DHA-GP</option>
                                        <option value="DHA-Specialist">DHA-Specialist</option>
                                        <option value="DHA-Dental Hygiene">DHA-Dental Hygiene</option>
                                        <option value="DHA-Lab Technician">DHA-Lab Technician</option>
                                        <option value="DHA-Lab Technologist">DHA-Lab Technologist</option>
                                        <option value="DHA-Pharamcist">DHA-Pharamcist</option>
                                        <option value="DHA-Pharamacy Technician">DHA-Pharamacy TechnicianGP</option>
                                  
                                      
                                    </select>
            @if ($errors->has('applied_for'))
                <span class="help-block">
                    <strong> {{ $errors->first('applied_for') }}</strong>
                </span>
            @endif

        </div>
    </div>
   
    <div class="form-group" {{ $errors->has('first_payment') ? ' has-error' : '' }}>
     <label for="first_payment" class="col-sm-2 control-label">first_payment:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>
        <div class="col-sm-8">
            {!! Form::text('first_payment', null , ['class'=> 'form-control', 'placeholder' => ' first_payment', 'id'=>"first_payment"]) !!}

            @if ($errors->has('first_payment'))
                <span class="help-block">
                    <strong> {{ $errors->first('first_payment') }}</strong>
                </span>
            @endif

        </div>
    </div>
  
    

    @if(Request::segment(4) == 'edit')
        <div class="form-group">
            <label for="status" class="col-sm-2 control-label">Status<span class=help-block" style="color: #b30000">&nbsp;* </span></label>

            <div class="col-sm-8">

                <label class="radio-inline"><input type="radio" id="active" name="status" value=1
                                                   @if($dhaFee->status==1) checked @endif >Active</label>&nbsp;
                <label class="radio-inline"><input type="radio" id="inactive" name="status" value=0
                                                   @if($dhaFee->status==0) checked @endif >Inactive</label>
            </div>
        </div>
    @endif
</div>
