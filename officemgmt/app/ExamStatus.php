<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExamStatus extends Model
{
   protected $fillable=['dha_id','preferred_date_from','preferred_date_to','dha/mcq','exam_date_booked','dha_status','eligibility'];
   public function dhafees(){
    	return $this->belongsTo('App\DhaFees');
    }
}
