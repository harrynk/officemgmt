<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DhaFees extends Model
{
    protected $fillable=['name','qualification','applied_for','first_payment','is_edit','total'];
      public function Report()
   {
   	return $this->hasMany('App\Report');
   }
   public function dhastatus(){
   	return $this->hasMany('App\DhaStatus');
   }
}
