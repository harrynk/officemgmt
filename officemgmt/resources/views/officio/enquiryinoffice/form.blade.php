<div class="box-body">

        <div class="form-group" {{ $errors->has('date') ? ' has-error' : '' }}>
            <label for="date" class="col-sm-2 control-label">Date:<span class=help-block"
                style="color: #b30000">&nbsp;* </span></label>

                <div class="col-sm-8">
                    {!! Form::date('date', null , ['class'=> 'form-control', 'placeholder' => ' date', 'id'=>"date"]) !!}

                    @if ($errors->has('date'))
                    <span class="help-block">
                        <strong> {{ $errors->first('date') }}</strong>
                    </span>
                    @endif

                </div>
            </div>
    <div class="form-group" {{ $errors->has('name') ? ' has-error' : '' }}>
        <label for="name" class="col-sm-2 control-label">name:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">
          {!! Form::text('name', null , ['class'=> 'form-control', 'placeholder' => ' name', 'id'=>"name"]) !!}

            @if ($errors->has('name'))
                <span class="help-block">
                    <strong> {{ $errors->first('name') }}</strong>
                </span>
            @endif

        </div>
    </div>

        <div class="form-group" {{ $errors->has('email') ? ' has-error' : '' }}>
            <label for="email" class="col-sm-2 control-label">email:<span class=help-block"
                style="color: #b30000">&nbsp;* </span></label>

                <div class="col-sm-8">
                    {!! Form::text('email', null , ['class'=> 'form-control', 'placeholder' => ' email', 'id'=>"email"]) !!}

                    @if ($errors->has('email'))
                    <span class="help-block">
                        <strong> {{ $errors->first('email') }}</strong>
                    </span>
                    @endif

                </div>
            </div>

       <div class="form-group" {{ $errors->has('qualification') ? ' has-error' : '' }}>
                        <label for="qualification" class="col-sm-2 control-label">qualification:<span class=help-block"
                            style="color: #b30000">&nbsp;* </span></label>

                            <div class="col-sm-8">
                                {!! Form::text('qualification', null , ['class'=> 'form-control', 'placeholder' => ' qualification', 'id'=>"qualification"]) !!}

                                @if ($errors->has('qualification'))
                                <span class="help-block">
                                    <strong> {{ $errors->first('qualification') }}</strong>
                                </span>
                                @endif

                            </div>
                        </div>
    <div class="form-group" {{ $errors->has('phone') ? ' has-error' : '' }}>
        <label for="phone" class="col-sm-2 control-label">phone:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">
            {!! Form::text('phone', null , ['class'=> 'form-control', 'placeholder' => ' phone', 'id'=>"phone"]) !!}

            @if ($errors->has('phone'))
                <span class="help-block">
                    <strong> {{ $errors->first('phone') }}</strong>
                </span>
            @endif

        </div>
    </div>
   
    <div class="form-group" {{ $errors->has('remarks') ? ' has-error' : '' }}>
     <label for="remarks" class="col-sm-2 control-label">remarks:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>
        <div class="col-sm-8">
            {!! Form::text('remarks', null , ['class'=> 'form-control', 'placeholder' => ' remarks', 'id'=>"remarks"]) !!}

            @if ($errors->has('remarks'))
                <span class="help-block">
                    <strong> {{ $errors->first('remarks') }}</strong>
                </span>
            @endif

        </div>
    </div>
  
    

    @if(Request::segment(4) == 'edit')
        <div class="form-group">
            <label for="status" class="col-sm-2 control-label">Status<span class=help-block" style="color: #b30000">&nbsp;* </span></label>

            <div class="col-sm-8">

                <label class="radio-inline"><input type="radio" id="active" name="status" value=1
                                                   @if($enquiryinoffice->status==1) checked @endif >Active</label>&nbsp;
                <label class="radio-inline"><input type="radio" id="inactive" name="status" value=0
                                                   @if($enquiryinoffice->status==0) checked @endif >Inactive</label>
            </div>
        </div>
    @endif
</div>
