    <div class="form-group" {{ $errors->has('dha_id') ? ' has-error' : '' }}>
        <label for="dha_id" class="col-sm-2 control-label">Preferred Date From:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">
          {!! Form::hidden('dha_id', null , ['class'=> 'form-control', 'placeholder' => ' dha_id', 'id'=>"dha_id"]) !!}

            @if ($errors->has('dha_id'))
                <span class="help-block">
                    <strong> {{ $errors->first('dha_id') }}</strong>
                </span>
            @endif

        </div>
    </div>
<div class="box-body">
  <div class="form-group" {{ $errors->has('email') ? ' has-error' : '' }}>
    <label for="email" class="col-sm-2 control-label">email:<span class=help-block"
      style="color: #b30000">&nbsp;* </span></label>

      <div class="col-sm-8">
        {!! Form::email('email', null , ['class'=> 'form-control', 'placeholder' => ' email', 'id'=>"email"]) !!}

        @if ($errors->has('email'))
        <span class="help-block">
          <strong> {{ $errors->first('email') }}</strong>
        </span>
        @endif


      </div>
    </div>
    
    <div class="form-group" {{ $errors->has('username') ? ' has-error' : '' }}>
      <label for="username" class="col-sm-2 control-label">username:<span class=help-block"
        style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">
          {!! Form::text('username', null , ['class'=> 'form-control', 'placeholder' => ' username', 'id'=>"username"]) !!}

          @if ($errors->has('username'))
          <span class="help-block">
            <strong> {{ $errors->first('username') }}</strong>
          </span>
          @endif

        </div>
      </div>

      <div class="form-group" {{ $errors->has('password') ? ' has-error' : '' }}>
        <label for="password" class="col-sm-2 control-label">password:<span class=help-block"
          style="color: #b30000">&nbsp;* </span></label>

          <div class="col-sm-8">
          <!--   {!! Form::text('password', null , ['class'=> 'form-control', 'placeholder' => ' password', 'id'=>"password"]) !!} -->
 {!! Form::input('password', 'password', null , ['class'=> 'form-control', 'placeholder' => 'Password', 'id'=>"password"]) !!}
            @if ($errors->has('password'))
            <span class="help-block">
              <strong> {{ $errors->first('password') }}</strong>
            </span>
            @endif

          </div>
        </div>






        @if(Request::segment(4) == 'edit')
        <div class="form-group">
          <label for="status" class="col-sm-2 control-label">Status<span class=help-block" style="color: #b30000">&nbsp;* </span></label>

          <div class="col-sm-8">

            <label class="radio-inline"><input type="radio" id="active" name="status" value=1
             @if($enquiry->status==1) checked @endif >Active</label>&nbsp;
             <label class="radio-inline"><input type="radio" id="inactive" name="status" value=0
               @if($enquiry->status==0) checked @endif >Inactive</label>
             </div>
           </div>
           @endif
         </div>
