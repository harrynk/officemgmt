<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CashOut;
use DB;
class CashOutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cashOuts = CashOut::all();
        $data=DB::table("cash_outs")->sum('amount');
        return view('officio.cashout.index',compact('cashOuts','data'))->with('title','Cash Out');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\ResponseC
     */
    public function create()
    {
        return view('officio.cashout.create')->with('title','Create Cash Out');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     
        $this->validate($request, [
            'expenses_type' => 'required',
            'amount' => 'required',
            'paid_by' => 'required',
            'received_by' => 'required',
            'payment_mode' => 'required',
           
        ]);
        $input = $request->all();
        if ($input) {
            CashOut::create($input);
            session()->flash('message', 'Cash Out Created.');
            return redirect('admin/cash_out');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cashOut = CashOut::find($id);
        return view('officio.cashout.edit',compact('cashOut'))->with('title','Edit Cash Out');
    }

    /**
     * Update the specified rescashOutource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'expenses_type' => 'required',
            'amount' => 'required',
            'paid_by' => 'required',
            'received_by' => 'required',
            'payment_mode' => 'required',
        ]);
        $cashOut = CashOut::find($id);
        $input = $request->all();
        $cashOut->update($input);
        session()->flash('message', 'Cash Out Updated.');
        return redirect('admin/cash_out');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!request()->ajax()){
            return false;
        }

        CashOut::find($id)->delete();


        session()->flash('message', 'Cash Out Deleted.');

        return response()->json(array(
            'status' => 'success',
        ));
    }
}

