<div class="box-body">

    
    <div class="form-group" {{ $errors->has('name') ? ' has-error' : '' }}>
        <label for="name" class="col-sm-2 control-label">name:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">
          {!! Form::text('name', null , ['class'=> 'form-control', 'placeholder' => ' name', 'id'=>"name"]) !!}

            @if ($errors->has('name'))
                <span class="help-block">
                    <strong> {{ $errors->first('name') }}</strong>
                </span>
            @endif

        </div>
    </div>

        <div class="form-group" {{ $errors->has('description') ? ' has-error' : '' }}>
            <label for="description" class="col-sm-2 control-label">description:<span class=help-block"
                style="color: #b30000">&nbsp;* </span></label>

                <div class="col-sm-8">
                    {!! Form::text('description', null , ['class'=> 'form-control', 'placeholder' => ' description', 'id'=>"description"]) !!}

                    @if ($errors->has('description'))
                    <span class="help-block">
                        <strong> {{ $errors->first('description') }}</strong>
                    </span>
                    @endif

                </div>
            </div>

      
    @if(Request::segment(4) == 'edit')
        <div class="form-group">
            <label for="status" class="col-sm-2 control-label">Status<span class=help-block" style="color: #b30000">&nbsp;* </span></label>

            <div class="col-sm-8">

                <label class="radio-inline"><input type="radio" id="active" name="status" value=1
                                                   @if($enquiry->status==1) checked @endif >Active</label>&nbsp;
                <label class="radio-inline"><input type="radio" id="inactive" name="status" value=0
                                                   @if($enquiry->status==0) checked @endif >Inactive</label>
            </div>
        </div>
    @endif
</div>
