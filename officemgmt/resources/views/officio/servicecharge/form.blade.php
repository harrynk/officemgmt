<div class="box-body">
     <div class="form-group" {{ $errors->has('name') ? ' has-error' : '' }}>
        <label for="name" class="col-sm-2 control-label">name:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">
            {!! Form::text('name', null , ['class'=> 'form-control', 'placeholder' => ' name', 'id'=>"name"]) !!}

            @if ($errors->has('name'))
                <span class="help-block">
                    <strong> {{ $errors->first('name') }}</strong>
                </span>
            @endif

        </div>
    </div>

       <div class="form-group" {{ $errors->has('qualification') ? ' has-error' : '' }}>
                        <label for="qualification" class="col-sm-2 control-label">qualification:<span class=help-block"
                            style="color: #b30000">&nbsp;* </span></label>

                            <div class="col-sm-8">
                                {!! Form::text('qualification', null , ['class'=> 'form-control', 'placeholder' => ' qualification', 'id'=>"qualification"]) !!}

                                @if ($errors->has('qualification'))
                                <span class="help-block">
                                    <strong> {{ $errors->first('qualification') }}</strong>
                                </span>
                                @endif

                            </div>
                        </div>
    <div class="form-group" {{ $errors->has('applied_for') ? ' has-error' : '' }}>
        <label for="applied_for" class="col-sm-2 control-label">applied_for:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">
            {!! Form::text('applied_for', null , ['class'=> 'form-control', 'placeholder' => ' applied_for', 'id'=>"applied_for"]) !!}

            @if ($errors->has('applied_for'))
                <span class="help-block">
                    <strong> {{ $errors->first('applied_for') }}</strong>
                </span>
            @endif

        </div>
    </div>
   
    <div class="form-group" {{ $errors->has('service_charge') ? ' has-error' : '' }}>
        <label for="service_charge" class="col-sm-2 control-label">service_charge:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">
            {!! Form::text('service_charge', null , ['class'=> 'form-control', 'placeholder' => ' service_charge', 'id'=>"service_charge"]) !!}

            @if ($errors->has('service_charge'))
                <span class="help-block">
                    <strong> {{ $errors->first('service_charge') }}</strong>
                </span>
            @endif

        </div>
    </div>
  


  

    @if(Request::segment(4) == 'edit')
        <div class="form-group">
            <label for="status" class="col-sm-2 control-label">Status<span class=help-block" style="color: #b30000">&nbsp;* </span></label>

            <div class="col-sm-8">

                <label class="radio-inline"><input type="radio" id="active" name="status" value=1
                                                   @if($serviceCharge->status==1) checked @endif >Active</label>&nbsp;
                <label class="radio-inline"><input type="radio" id="inactive" name="status" value=0
                                                   @if($serviceCharge->status==0) checked @endif >Inactive</label>
            </div>
        </div>
    @endif
</div>
