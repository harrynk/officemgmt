<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DhaFees;
use App\DhaStatus;
use App\DataflowStatus;
use DB;
use App\User;
class StatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $status = DhaStatus::all();
    $dha_fees=DhaFees::pluck('name','id');
        return view('officio.status.index',compact('status','dha_fees'))->with('title','User Status');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\ResponseC
     */
    public function create()
    {
    	// $dhaFees_detail=DhaFees::all();
    	$dhafees=DhaFees::pluck('name','id');
        return view('officio.status.create',compact('dhafees'))->with('title','Create User Status');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     dd($request);
        $this->validate($request, [
            'dha_id' => 'required',
           
           
        ]);
        $input = $request->all();
        if ($input) {
           DhaStatus::create($input);
            session()->flash('message', 'User Status  Created.');
            return redirect('admin/dhastatus');
            // return view('')
        }
    }
public function createdhastatus()
    {
        // $dhaFees_detail=DhaFees::all();
        $dataflow=DataflowStatus::all();
        $dhastatus=DhaStatus::all();
        $users=User::all();
        $dhafees=DhaFees::all();
        return view('officio.status.createdhastatus',compact('dhafees','users','dhastatus','dataflow'))->with('title','Create User Status');
    }
    public function postdhastatus(Request $request){
       $this->validate($request, [
            'email' => 'required',
            'username' => 'required',
            'password' => 'required',
           
           
        ]);

        $input = $request->all();
          $input['password']=bcrypt($request->password);
        if ($input) {
           DhaStatus::create($input);
            session()->flash('message', 'User Status  Created.');
            return redirect('admin/status');
        }   
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $cashOut = CashOut::find($id);
         $dhastatus=DhaStatus::find($id);
        $users=User::all();
        $dhafees=DhaFees::all();  
        return view('officio.status.edit',compact('dhastatus','users','dhafees'))->with('title','Edit User Status');
    }

    /**
     * Update the specified rescashOutource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request, $id)
    // {
    //     $this->validate($request, [
    //         'expenses_type' => 'required',
    //         'amount' => 'required',
    //         'paid_by' => 'required',
    //         'received_by' => 'required',
    //         'payment_mode' => 'required',
    //     ]);
    //     $cashOut = CashOut::find($id);
    //     $input = $request->all();
    //     $cashOut->update($input);
    //     session()->flash('message', 'Cash Out Updated.');
    //     return redirect('admin/cash_out');
    // }

    // /**
    //  * Remove the specified resource from storage.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function destroy($id)
    // {
    //     if(!request()->ajax()){
    //         return false;
    //     }

    //     CashOut::find($id)->delete();


    //     session()->flash('message', 'Cash Out Deleted.');

    //     return response()->json(array(
    //         'status' => 'success',
    //     ));
    // }
}

