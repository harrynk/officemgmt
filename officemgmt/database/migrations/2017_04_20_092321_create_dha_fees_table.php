<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDhaFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dha_fees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('qualification');
            $table->string('applied_for');           
            $table->string('first_payment');           
            $table->string('is_edit')->nullable();           
            $table->float('total')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dha_fees');
    }
}
