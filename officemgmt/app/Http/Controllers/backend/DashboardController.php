<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Redirect;
// use redirect;
use App\User;
use Validator;
use Hash;
use Carbon\Carbon;
use App\Cash_in;
use App\CashOut;
use App\DhaFees;<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Redirect;
// use redirect;
use App\User;
use Validator;
use Hash;
use Carbon\Carbon;
use App\Cash_in;
use App\CashOut;
use App\DhaFees;
use DB;
use App\Report;

use App\ServiceCharge;
use Illuminate\Support\Facades\Input;


class DashboardController extends Controller
{
  public function __construct(){
    $this->middleware('auth',['only'=>'index']);
    $this->middleware('guest',['only'=>'getLogin']);
  }

  public function index(){

    $cashins=Cash_in::pluck('service_charge','id');
    $data_cashin = DB::table("cash_ins")->sum('service_charge');

    $cashouts=CashOut::pluck('amount','id');
    $data_cashout=DB::table("cash_outs")->sum('amount');

    $dhaFees = DhaFees::pluck('first_payment','id');
    $data_dhafees=DB::table("dha_fees")->sum('first_payment');
    $serviceCharges = ServiceCharge::pluck('service_charge','id');
    $data_servicecharge=DB::table("service_charges")->sum('service_charge');

    $timestemp = \Carbon\Carbon::now()->toDateTimeString();
    $month = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $timestemp)->month;


    return view('officio.dashboard.index',compact('cashins','cashouts','dhafees','serviceCharges','reports','current_time',' data_cashin','data_cashout','data_dhafees','data_servicecharge','month'))->with('title','Green Computing Nepal | Dashboard');
  }
  public function getReport(){
    $month=$_POST['month'];
    $rent = DB::table('cash_outs')->select(DB::raw('SUM(amount) as amount'))->where('expenses_type','=','Rent')->whereMonth('created_at', '=', $month)->get();

    $electricity = DB::table('cash_outs')->select(DB::raw('SUM(amount) as amount'))->where('expenses_type','=','Electricity & Garbage')->whereMonth('created_at', '=', $month)->get();
    $internet = DB::table('cash_outs')->select(DB::raw('SUM(amount) as amount'))->where('expenses_type','=','Internet')->whereMonth('created_at', '=', $month)->get();

    $book = DB::table('cash_outs')->select(DB::raw('SUM(amount) as amount'))->where('expenses_type','=','Book Printing')->whereMonth('created_at', '=', $month)->get();
    $advertising = DB::table('cash_outs')->select(DB::raw('SUM(amount) as amount'))->where('expenses_type','=','Advertising & Marketing')->whereMonth('created_at', '=', $month)->get();
    $cancellation = DB::table('cash_outs')->select(DB::raw('SUM(amount) as amount'))->where('expenses_type','=','Cancellation/Refund')->whereMonth('created_at', '=', $month)->get();
    $telephone = DB::table('cash_outs')->select(DB::raw('SUM(amount) as amount'))->where('expenses_type','=','Telephone')->whereMonth('created_at', '=', $month)->get();
    $stationery = DB::table('cash_outs')->select(DB::raw('SUM(amount) as amount'))->where('expenses_type','=','Stationery')->whereMonth('created_at', '=', $month)->get();
    $lunch = DB::table('cash_outs')->select(DB::raw('SUM(amount) as amount'))->where('expenses_type','=','Lunch')->whereMonth('created_at', '=', $month)->get();
    $snacks = DB::table('cash_outs')->select(DB::raw('SUM(amount) as amount'))->where('expenses_type','=','Snacks & tea')->whereMonth('created_at', '=', $month)->get();
    $staff = DB::table('cash_outs')->select(DB::raw('SUM(amount) as amount'))->where('expenses_type','=','Staff Salary')->whereMonth('created_at', '=', $month)->get();
    $fuel = DB::table('cash_outs')->select(DB::raw('SUM(amount) as amount'))->where('expenses_type','=','Fuel & travel expenses')->whereMonth('created_at', '=', $month)->get();
    
    $repair = DB::table('cash_outs')->select(DB::raw('SUM(amount) as amount'))->where('expenses_type','=','Repair & maintenance')->whereMonth('created_at', '=', $month)->get();
    $loan = DB::table('cash_outs')->select(DB::raw('SUM(amount) as amount'))->where('expenses_type','=','Loan Payment')->whereMonth('created_at', '=', $month)->get();
    $basic= DB::table('cash_outs')->select(DB::raw('SUM(amount) as amount'))->where('expenses_type','=','Basic Life Support')->whereMonth('created_at', '=', $month)->get();
    $others = DB::table('cash_outs')->select(DB::raw('SUM(amount) as amount'))->where('expenses_type','=','Others')->whereMonth('created_at', '=', $month)->get();

    $data_cashin = DB::table('cash_ins')->select(DB::raw('SUM(service_charge) as service_charge'))->whereMonth('created_at', '=', $month)->get();
    $data_cashouts = DB::table('cash_outs')->select(DB::raw('SUM(amount) as amount'))->whereMonth('created_at', '=', $month)->get();
    $services = DB::table('service_charges')->select(DB::raw('SUM(service_charge) as service_charge'))->whereMonth('created_at', '=', $month)->get();
    $dhafees = DB::table('dha_fees')->select(DB::raw('SUM(first_payment) as first_payment'))->whereMonth('created_at', '=', $month)->get();
    return view('officio.dashboard.report',compact('data_cashin','month','data_cashouts','services','dhafees','expenses','rent','electricity','internet','book','advertising','cancellation','telephone','stationery','lunch','snacks','staff','repair','fuel','loan','basic','others','loan'))->with('title','Show record');
  }

  public function nepalidate(){
    $month=$_POST['month'];
    $rent = DB::table('cash_outs')->select(DB::raw('SUM(amount) as amount'))->where('expenses_type','=','Rent')->whereMonth('created_at', '=', $month)->get();

    $electricity = DB::table('cash_outs')->select(DB::raw('SUM(amount) as amount'))->where('expenses_type','=','Electricity & Garbage')->whereMonth('created_at', '=', $month)->get();
    $internet = DB::table('cash_outs')->select(DB::raw('SUM(amount) as amount'))->where('expenses_type','=','Internet')->whereMonth('created_at', '=', $month)->get();

    $book = DB::table('cash_outs')->select(DB::raw('SUM(amount) as amount'))->where('expenses_type','=','Book Printing')->whereMonth('created_at', '=', $month)->get();
    $advertising = DB::table('cash_outs')->select(DB::raw('SUM(amount) as amount'))->where('expenses_type','=','Advertising & Marketing')->whereMonth('created_at', '=', $month)->get();
    $cancellation = DB::table('cash_outs')->select(DB::raw('SUM(amount) as amount'))->where('expenses_type','=','Cancellation/Refund')->whereMonth('created_at', '=', $month)->get();
    $telephone = DB::table('cash_outs')->select(DB::raw('SUM(amount) as amount'))->where('expenses_type','=','Telephone')->whereMonth('created_at', '=', $month)->get();
    $stationery = DB::table('cash_outs')->select(DB::raw('SUM(amount) as amount'))->where('expenses_type','=','Stationery')->whereMonth('created_at', '=', $month)->get();
    $lunch = DB::table('cash_outs')->select(DB::raw('SUM(amount) as amount'))->where('expenses_type','=','Lunch')->whereMonth('created_at', '=', $month)->get();
    $snacks = DB::table('cash_outs')->select(DB::raw('SUM(amount) as amount'))->where('expenses_type','=','Snacks & tea')->whereMonth('created_at', '=', $month)->get();
    $staff = DB::table('cash_outs')->select(DB::raw('SUM(amount) as amount'))->where('expenses_type','=','Staff Salary')->whereMonth('created_at', '=', $month)->get();
    $fuel = DB::table('cash_outs')->select(DB::raw('SUM(amount) as amount'))->where('expenses_type','=','Fuel & travel expenses')->whereMonth('created_at', '=', $month)->get();
    
    $repair = DB::table('cash_outs')->select(DB::raw('SUM(amount) as amount'))->where('expenses_type','=','Repair & maintenance')->whereMonth('created_at', '=', $month)->get();
    $loan = DB::table('cash_outs')->select(DB::raw('SUM(amount) as amount'))->where('expenses_type','=','Loan Payment')->whereMonth('created_at', '=', $month)->get();
    $basic= DB::table('cash_outs')->select(DB::raw('SUM(amount) as amount'))->where('expenses_type','=','Basic Life Support')->whereMonth('created_at', '=', $month)->get();
    $others = DB::table('cash_outs')->select(DB::raw('SUM(amount) as amount'))->where('expenses_type','=','Others')->whereMonth('created_at', '=', $month)->get();

    $data_cashin = DB::table('cash_ins')->select(DB::raw('SUM(service_charge) as service_charge'))->whereMonth('created_at', '=', $month)->get();
    $data_cashouts = DB::table('cash_outs')->select(DB::raw('SUM(amount) as amount'))->whereMonth('created_at', '=', $month)->get();
    $services = DB::table('service_charges')->select(DB::raw('SUM(service_charge) as service_charge'))->whereMonth('created_at', '=', $month)->get();
    $dhafees = DB::table('dha_fees')->select(DB::raw('SUM(first_payment) as first_payment'))->whereMonth('created_at', '=', $month)->get();
    return view('officio.dashboard.report',compact('data_cashin','month','data_cashouts','services','dhafees','expenses','rent','electricity','internet','book','advertising','cancellation','telephone','stationery','lunch','snacks','staff','repair','fuel','loan','basic','others','loan'))->with('title','Show record');

  }
  public function getLogin(){
    return view('admin.login')->with('title','Green Commputing Nepal | Admin Login');
  }

  public function postLogin(Request $request){
       // dd($request);
    if(Auth::attempt([
      'username' => $request->username,
      'password'=>$request->password
      ])){

      $user_id = auth()->user()->id;
    return redirect('admin/'.$user_id.'/dashboard');
  }

// return redirect()->back();
  Auth::logout();
  return redirect('admin')
  ->withInput($request->only('username'))
  ->withErrors([
    'username' => 'Invalid username/password'
    ]);
}

public function getLogout(){
  Auth::logout();
  return redirect('admin')->with('title','Aladdin | Admin Login');
}

public function profile(){
  $users = User::find(Auth::user()->id);

  return view('officio.profile', array('users'=>$users))->with('title','Aladdin | Profile');
}

public function editInfo(Request $request){
  $this->validate($request, [
    'name'     => 'required',

    'username'     => 'required',
    ]); 

  $id = Auth::user()->id;
  $user = User::find($id);


  $user->name = $request->input('name');
  $user->username = $request->input('username');



  $user->save();
  session()->flash('message', 'Profile Updated.');
  return redirect('admin/profile');

}


public function updatePassword(Request $request)
{
 $user = Auth::user();

 $this->validate($request, [
  'old_password'          => 'required',
  'new_password'                 => 'required|min:6',
  'confirm_password'    => 'required|same:new_password'
  ]);

 $user->password = Hash::make(Input::get('new_password'));
 $user->save();
 session()->flash('message', 'Profile Updated.');

 return redirect()->back();
}




}


use DB;
use App\Report;

use App\ServiceCharge;
use Illuminate\Support\Facades\Input;


class DashboardController extends Controller
{
  public function __construct(){
    $this->middleware('auth',['only'=>'index']);
    $this->middleware('guest',['only'=>'getLogin']);
  }

  public function index(){
    
    $cashins=Cash_in::pluck('service_charge','id');
    $data_cashin = DB::table("cash_ins")->sum('service_charge');

    $cashouts=CashOut::pluck('amount','id');
    $data_cashout=DB::table("cash_outs")->sum('amount');

    $dhaFees = DhaFees::pluck('first_payment','id');
    $data_dhafees=DB::table("dha_fees")->sum('first_payment');
    $serviceCharges = ServiceCharge::pluck('service_charge','id');
    $data_servicecharge=DB::table("service_charges")->sum('service_charge');

    $timestemp = \Carbon\Carbon::now()->toDateTimeString();
$month = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $timestemp)->month;

   
    return view('officio.dashboard.index',compact('cashins','cashouts','dhafees','serviceCharges','reports','current_time',' data_cashin','data_cashout','data_dhafees','data_servicecharge','month'))->with('title','Green Computing Nepal | Dashboard');
  }
  public function getReport(){

   // $data_cashin = DB::table('cash_ins')->select(DB::raw('SUM(service_charge) as Service_charge'))->whereMonth('created_at', '=', 5)->get();
  $data_cashin = DB::table('cash_ins')->select(DB::raw('SUM(service_charge) as Service_charge'))->where('created_at', '>=', Carbon::now()->startOfMonth())->get();
// echo "$data_cashin";
     return view('officio.dashboard.report',compact('data_cashin','month'))->with('title','Show records');
  }
  public function getLogin(){
    return view('admin.login')->with('title','Green Commputing Nepal | Admin Login');
  }

  public function postLogin(Request $request){
       // dd($request);
    if(Auth::attempt([
      'username' => $request->username,
      'password'=>$request->password
      ])){

      $user_id = auth()->user()->id;
    return redirect('admin/'.$user_id.'/dashboard');
  }

// return redirect()->back();
  Auth::logout();
  return redirect('admin')
  ->withInput($request->only('username'))
  ->withErrors([
    'username' => 'Invalid username/password'
    ]);
}

public function getLogout(){
  Auth::logout();
  return redirect('admin')->with('title','Aladdin | Admin Login');
}

public function profile(){
  $users = User::find(Auth::user()->id);

  return view('officio.profile', array('users'=>$users))->with('title','Aladdin | Profile');
}

public function editInfo(Request $request){
  $this->validate($request, [
    'name'     => 'required',

    'username'     => 'required',
    ]); 

  $id = Auth::user()->id;
  $user = User::find($id);


  $user->name = $request->input('name');
  $user->username = $request->input('username');



  $user->save();
  session()->flash('message', 'Profile Updated.');
  return redirect('admin/profile');

}


public function updatePassword(Request $request)
{
 $user = Auth::user();

 $this->validate($request, [
  'old_password'          => 'required',
  'new_password'                 => 'required|min:6',
  'confirm_password'    => 'required|same:new_password'
  ]);

 $user->password = Hash::make(Input::get('new_password'));
 $user->save();
 session()->flash('message', 'Profile Updated.');

 return redirect()->back();
}




}

