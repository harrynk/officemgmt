<?php
namespace App\Http\Controllers\backend;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Offer;
use App\User;
use Validator;
use Redirect;
use Auth;
use Mail;
// use Request;
use session;

class OfferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $offers = Offer::all();
        
        return view('officio.offer.index',compact('offers'))->with('title','Griffith offer');
    }
    /**Griffith 
     * Show the form for creating a new resource.
     *ResponseC
     * @return \Illuminate\Http\
     */
    public function create()
    {
        return view('officio.offer.create')->with('title','Create Griffith offer');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [

            'name' => 'required',
            'title' => 'required',
            'description' => 'required',
            'address' => 'required',
            'percentage' => 'required',
            'image' => 'required',
            'email' => 'required',
            
            
            
            ]);
        $input = $request->all();
        if ($input) {
            Offer::create($input);
            session()->flash('message', 'Griffith offer Created.');
            return redirect('admin/offer');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $offers=Offer::find($id);
     $offers=Offer::all();
     // $users=Auth::user()->id;
     $users=User::all();
     return view('officio.offer.show',compact('offers','users'))->with('title','Griffith offer');
 }


 public function postOffer(Request $request){


     $data = $request->all();

       $data1 = $request->input('email');
 

        $check = Validator::make($data, array(

                 'first_name' => 'required',
        'last_name' => 'required',
        'email' => 'required',
        ));

        // $emails = [

        //     'palistha01@gmail.com'
        // ];

        // if the validator fails, redirect back to the form
        if ($check->fails()) {
            return Redirect::back()
                            ->withErrors($check) // send back all errors to the login form
                            ->withInput();
        } else {
            Mail::send('emails.contact', ['data' => $data], function ($message) use ($data,$data1){
                    
                $message->to($data1)->subject('Contact query');
              
            });
            $request->session()->flash('success', 'Contact request has been sent.');
            return redirect('admin/offer');
        }


                          
                        }


 // public function postoffer(){
 //    $data = Request::all();
 //       // var_dump( $data); die;
 //    $data1 = Request::input('email');
 //    // $checkboxes2 = Request::get('formemail');
 //    // $checkboxes3 = Request::get('inemail');
 // // dd($checkboxes1);
 //    $check = Validator::make($data, array(

 //        'first_name' => 'required',
 //        'last_name' => 'required',
 //        'email' => 'required',
 //        ));

 //    $emails = [

 //    'palistha01@gmail.com'
 //    ];

 //        // if the validator fails, redirect back to the form
 //    if ($check->fails()) {
 //        return Redirect::back()
 //                            ->withErrors($check) // send back all errors to the login form
 //                            ->withInput();
 //                        } else {
 //                           //  foreach ($data1 as $checkEmail1) {
 //                           //     $this->sendEmail($checkEmail1,$data);
 //                           // } 
                         


 //                           Request::session()->flash('success', 'Contact request has been sent.');
 //                           return redirect('admin/offer');
 //                       }


 //                   }
 //                   private function sendEmail($email_address,$data){
 //                    Mail::send('emails.contact', ['data' => $data], function ($message) use ($data,$email_address){
 //                       // $message->to( $request->input('usemail') );
 //                        $message->to($email_address)->subject('Contact query');
 //                    });
 //                }  
                public function edit($id)
                {
                    $offer = Offer::find($id);
                    return view('officio.offer.edit',compact('offer'))->with('title','Edit Griffith offer');
                }

    /**
     * Update the specified rescashOutource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
          'name' => 'required',
          'title' => 'required',
          'description' => 'required',
          'address' => 'required',
          'percentage' => 'required',
          'image' => 'required',
          'email' => 'required'
          
          ]);
        $offer = Offer::find($id);
        $input = $request->all();
        $offer->update($input);
        session()->flash('message', 'Griffith offer Updated.');
        return redirect('admin/offer');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!request()->ajax()){
            return false;
        }

        Offer::find($id)->delete();


        session()->flash('message', 'Griffith offer Deleted.');

        return response()->json(array(
            'status' => 'success',
            ));
    }
}

