@extends('officio.main')

@section('content')
{{----}}
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
       News Letter System
    </h1>
</section>
@include('officio.flash.message')
<script type="text/javascript">
    function ShowHideDivins(ins) {
        var tableins = document.getElementById("tableins");
        tableins.style.display = ins.checked ? "block" : "none";
    }
</script>
<script type="text/javascript">
    function ShowHideDivus(us) {
        var tableus = document.getElementById("tableus");
        tableus.style.display = us.checked ? "block" : "none";
    }
</script>
<script type="text/javascript">
    function ShowHideDivform(form) {
        var tableform = document.getElementById("tableform");
        tableform.style.display = form.checked ? "block" : "none";
    }
</script>
<style type="text/css">
 

</style>
<div id="page-wrapper" class="page-wrapper-cls">
            <div id="page-inner">
                <div class="row">
                  
                </div>
                <div class="row"> 
                <!-- News Letter System -->
                    <div class="col-md-9">
                        <div class="panel panel-default">
                    
                        <div class="panel-body">
                   {{ Form::open(['url'=>'admin/news', 'method'=>'POST', 'class' => 'form-horizontal','enctype'=>'multipart/form-data' ]) }}

     <div class="box-body">
                     <div class="form-group" {{ $errors->has('subject') ? ' has-error' : '' }}>
                        <label for="subject" class="col-sm-2 control-label">Subject:<span class=help-block"
                            style="color: #b30000">&nbsp;* </span></label>

                            <div class="col-sm-10">
                              {!! Form::text('subject', null , ['class'=> 'form-control', 'placeholder' => ' subject', 'id'=>"subject"]) !!}

                              @if ($errors->has('subject'))
                              <span class="help-block">
                                <strong> {{ $errors->first('subject') }}</strong>
                            </span>
                            @endif

                        </div>
                    </div>




                    <div class="form-group" {{ $errors->has('message') ? ' has-error' : '' }}>
                        <label for="message" class="col-sm-2 control-label">message:<span class=help-block"
                            style="color: #b30000">&nbsp;* </span></label>

                            <div class="col-sm-10">
                               <textarea class="form-control ckeditor" name="message"  id="message" >
                               </textarea>

                               @if ($errors->has('message'))
                               <span class="help-block">
                                   <strong> {{ $errors->first('message') }}</strong>
                               </span>
                               @endif

                           </div>
                       </div>
                       </div>
                        <br/>
  
 
                            </div>
                            </div>
                    </div>
                      <div class="col-md-3">
                     <!--   selection Table  -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           Instructions
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        
                                    </thead>
                                   <tbody>
                                        <tr>
                                          <td>1</td>
                                            <td>Enter Subject </td>
                                        </tr>
                                          <tr>
                                          <td>3</td>
                                            <td>Enter Message </td>
                                        </tr>
                                           <tr>
                                          <td>4</td>
                                            <td>Select Categories</td>
                                        </tr>
                                           <tr>
                                          <td>5</td>
                                            <td>Check Email List </td>
                                        </tr>
                                          <tr>
                                          <td>5</td>
                                            <td>Send Mail </td>
                                        </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                      <!-- End  selection Table  -->
                </div>
                
                <div class="col-md-9">
                      <!--    Striped Rows Table  -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           Category Name Email List
                        </div>
                        <div class="panel-body">

                            <div class="table-responsive">
                                <span><strong>Enquiry By Us</strong></span>
                                <table id="tableus" style="display:none;"  class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>&nbsp;&nbsp;All <input type="checkbox" id="checkAllus" class="checkbox pull-left"></th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Profession</th>
                                        </tr>
                                    </thead>
                                    <tbody id="myTable">
                                         <?php foreach($uss as $us){ ?>
                                        <tr>
                                            <td><input type="checkbox" name="usemail[]" value="<?php echo $us->email;?>"  class="checkbox"/></td>
                                            <td><?php echo $us->name; ?></td>
                                            <td><?php echo $us->email; ?></td>
                                            <td><?php echo $us->qualification; ?></td>
                                        </tr>
                                       <?php } ?>
                                    </tbody>
                                </table> 
                                </div>
                                <div class="col-md-12 text-center">
      <ul class="pagination pagination-sm pager" id="myPager"></ul>
      </div>
</div>

                            
                        <div class="panel-body">
                            <div class="table-responsive">
                               <strong>Enquiry Form</strong> 
                               <table id="tableform" style="display:none;" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>&nbsp;&nbsp;All <input type="checkbox" id="checkAllform"  class="checkbox pull-left"></th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Profession</th>
                                        </tr>
                                    </thead>
                                    <tbody id="myTable2">
                                       <?php foreach($forms as $form){ ?>
                                        <tr>
                                            <td><input type="checkbox" value="<?php echo $form->email;?>" name="formemail[]" class="checkbox"></td>
                                            <td><?php echo $form->name; ?></td>
                                            <td><?php echo $form->email; ?></td>
                                            <td><?php echo $form->qualification; ?></td>
                                        </tr>
                                       <?php } ?>                                       
                                    </tbody>
                                </table>
                                </div>
                                 <div class="col-md-12 text-center">
      <ul class="pagination pagination-sm pager" id="myPager2"></ul>
      </div>
</div>

                        <div class="panel-body">
                            <div class="table-responsive">
                                <strong>Enquiry In Office</strong>
                                <table id="tableins" style="display:none;"  class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>&nbsp;&nbsp;All <input type="checkbox" id="checkAllins" class="checkbox pull-left"></th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Profession</th>
                                        </tr>
                                    </thead>
                                     <tbody id="myTable3">
                                        <?php foreach($ins as $in){ ?>
                                        <tr>
                                            <td><input type="checkbox" value="<?php echo $in->email;?>" name="inemail[]" class="checkbox"></td>
                                            <td><?php echo $in->name; ?></td>
                                            <td><?php echo $in->email; ?></td>
                                            <td><?php echo $in->qualification; ?></td>
                                        </tr>
                                       <?php } ?> 
                                    </tbody>
                                </table>
                                </div>
                                 <div class="col-md-12 text-center">
      <ul class="pagination pagination-sm pager" id="myPager3"></ul>
      </div>
          </div>
                            </div>
                        </div>  
               <div class="col-md-3">
                     <!--   selection Table  -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Select Category For Sending Mail
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table hello">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Select Categories</th>
                                         </tr>
                                    </thead>
                                    <tbody id="myTable">
                                          <tr>
                                          <td><input type="checkbox" onclick="ShowHideDivus(this)" id="us" name="uss" class="checkbox" ></td>
                                            <td>Enquiry By Us</td>
                                         </tr>
                                         <tr>
                                          <td><input type="checkbox" onclick="ShowHideDivform(this)" id="form" name="forms" class="checkbox"></td>
                                            <td>Enquiry Form</td>
                                         </tr>
                                        <tr>
                                          <td><input type="checkbox" onclick="ShowHideDivins(this)" id="ins" name="inss" class="checkbox"></td>
                                            <td>Enquiry In</td>
                                         </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                      <!-- End  selection Table  -->
                </div>
                 <div class="col-offset-md-9 col-md-3">
                      <input type="submit" style="width: 100%; height: 90px; font-size: 25px;" class="btn btn-default" value="Send Mail"/>
                </div>
                </form>
            </div>
     
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  --


    <!-- all click -->
 
    <script type="text/javascript">
    $(document).ready(function() {
          $("#checkAllins").change(function () {
          $("#tableins input:checkbox").prop('checked', $(this).prop("checked"));
      });
    });
    </script>
    <script type="text/javascript">
    $( document ).ready(function() {
          $("#checkAllform").change(function () {
          $("#tableform input:checkbox").prop('checked', $(this).prop("checked"));
      });
    });
    </script>
    <script type="text/javascript">
    $( document ).ready(function() {
          $("#checkAllus").change(function () {
          $("#tableus input:checkbox").prop('checked', $(this).prop("checked"));

      });
    });
    </script>
   <!--  <script type="text/javascript">

     $("#all").validate({
      rules: {
        subject: "required",
        message: "required",
        
      },
      messages: {
        subject: "Please enter your subject",
        message: "Please enter your message",
      }
    });

</script> -->
    <script type="text/javascript">

$.fn.pageMe = function(opts){
    var $this = this,
        defaults = {
            perPage: 10,
            showPrevNext: false,
            hidePageNumbers: false
        },
        settings = $.extend(defaults, opts);
    
    var listElement = $this;
    var perPage = settings.perPage; 
    var children = listElement.children();
    var pager = $('.pager');
    
    if (typeof settings.childSelector!="undefined") {
        children = listElement.find(settings.childSelector);
    }
    
    if (typeof settings.pagerSelector!="undefined") {
        pager = $(settings.pagerSelector);
    }
    
    var numItems = children.size();
    var numPages = Math.ceil(numItems/perPage);

    pager.data("curr",0);
    
    if (settings.showPrevNext){
        $('<li><a href="#" class="prev_link">«</a></li>').appendTo(pager);
    }
    
    var curr = 0;
    while(numPages > curr && (settings.hidePageNumbers==false)){
        $('<li><a href="#" class="page_link">'+(curr+1)+'</a></li>').appendTo(pager);
        curr++;
    }
    
    if (settings.showPrevNext){
        $('<li><a href="#" class="next_link">»</a></li>').appendTo(pager);
    }
    
    pager.find('.page_link:first').addClass('active');
    pager.find('.prev_link').hide();
    if (numPages<=1) {
        pager.find('.next_link').hide();
    }
    pager.children().eq(1).addClass("active");
    
    children.hide();
    children.slice(0, perPage).show();
    
    pager.find('li .page_link').click(function(){
        var clickedPage = $(this).html().valueOf()-1;
        goTo(clickedPage,perPage);
        return false;
    });
    pager.find('li .prev_link').click(function(){
        previous();
        return false;
    });
    pager.find('li .next_link').click(function(){
        next();
        return false;
    });
    
    function previous(){
        var goToPage = parseInt(pager.data("curr")) - 1;
        goTo(goToPage);
    }
     
    function next(){
        goToPage = parseInt(pager.data("curr")) + 1;
        goTo(goToPage);
    }
    
    function goTo(page){
        var startAt = page * perPage,
            endOn = startAt + perPage;
        
        children.css('display','none').slice(startAt, endOn).show();
        
        if (page>=1) {
            pager.find('.prev_link').show();
        }
        else {
            pager.find('.prev_link').hide();
        }
        
        if (page<(numPages-1)) {
            pager.find('.next_link').show();
        }
        else {
            pager.find('.next_link').hide();
        }
        
        pager.data("curr",page);
        pager.children().removeClass("active");
        pager.children().eq(page+1).addClass("active");
    
    }
};

$(document).ready(function(){
    
  $('#myTable').pageMe({pagerSelector:'#myPager',showPrevNext:true,hidePageNumbers:true,perPage:10});
    
});
$(document).ready(function(){
    
  $('#myTable2').pageMe({pagerSelector:'#myPager2',showPrevNext:true,hidePageNumbers:true,perPage:10});
    
});
$(document).ready(function(){
    
  $('#myTable3').pageMe({pagerSelector:'#myPager3',showPrevNext:true,hidePageNumbers:true,perPage:10});
    
});
    </script>
    </section>
@stop