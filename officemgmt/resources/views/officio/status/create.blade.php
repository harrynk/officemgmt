@extends('officio.main')
@section('title','add User Status')

@section('content')
{{----}}
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
      Create Dubai Health Authority(DHA) Profile
    </h1>
</section>
@include('officio.flash.message')


<section class="content">
   <div class="row">
    <div class="col-md-12">

        {{--@include('flash.message')--}}
        {{--@include('errors.list')--}}

        <!-- Horizontal Form -->
        <div class="box box-info">
            <!-- form start -->
            <div class="box-body">
                <div class="row">
                  <div class="col-md-8">
                    {{ Form::open(['url'=>'admin/status', 'method'=>'POST', 'class' => 'form-horizontal','enctype'=>'multipart/form-data' ]) }}
                    @include('officio.status.form')
                    <div class="text-right border-top">
                        <button type="submit" class="btn btn-warning">Create Profile</button>
                    </div>
                    {{ form::close() }}
                </div>
            </div>
        </div>
    </div><!-- /.box -->


</div>
</section>
  <style type="text/css">

        ul#minitabs {
            list-style: none;
            margin: 0;
            padding: 7px 0;
            border-bottom: 1px solid #CCC;
            font-weight: bold;
            text-align: center;
            white-space: nowrap
        }
        ul#minitabs li {
            display: inline;
            margin: 0 3px;
            padding: 0 40px 0 0;
        }
        ul#minitabs a {
            text-decoration: none;
            padding: 0 0 3px;
            border-bottom: 4px solid #f7f5f5;
            /* color: #999; */
            color: #008000;
        }

        ul#minitabs a.current {
            border-color: #F60;
            /*color:#06F;*/
            color: #337ab7;
        }

        ul#minitabs a:hover {
            border-color: #F60;
            color: #666;
        }

        .my-text {
            font-family: "Courier-new";
            font-size: 22px;
        }

        .my-icon, .alt-form, .pri-form, .case-form, .case-app-form, .address-history-form, .emp-form, .academic-form, .members-form, .old-passport-form, .other-name-form, .pre-aus-travel-form, .pre-other-travel-form, .visa-issue-form, .character-form, .medical-form, .health-form, .language-form, .police-form, .pre-relation-form, .task-form {
            vertical-align: middle;
            font-size: 20px;
        }

        .fa {
            background: none;
            margin: 3px 11px 10px 0 !important;
            border-radius: 1px solid #000 !important;
        }

 
    @media screen and (max-width: 700px){

        ul#minitabs {
            list-style: none;
            margin: 0;
            padding: 7px 3px;
            border-bottom: 1px solid #CCC;
            font-weight: bold;
            font-size:10px;
            text-align: center;
            white-space: nowrap
        }
        ul#minitabs li {
            display: inline;
            margin: 0 3px;
            padding: 0 6px 0 0;
        }
        ul#minitabs a {
            text-decoration: none;
            padding: 0 0 3px;
            border-bottom: 4px solid #FFF;
            color: #999
        }
        ul#minitabs a.current {
            border-color: #F60;
            color:#06F
        }
        ul#minitabs a:hover {
            border-color: #F60;
            color: #666
        }

        @media screen and (max-width: 480px){

            ul#minitabs {
                list-style: none;
                margin: 0;
                padding: 7px 10px 8px 0;
                border-bottom: 1px solid #CCC;
                font-weight: bold;
                font-size:8px;
                text-align: center;
                white-space: nowrap
            }
            ul#minitabs li {
                display: inline;
                margin: 0 3px;
                padding: 0 4px 0 0;
            }
            ul#minitabs a {
                text-decoration: none;
                padding: 0 0 3px;
                border-bottom: 4px solid #FFF;
                color: #999
            }
            ul#minitabs a.current {
                border-color: #F60;
                color:#06F
            }
            ul#minitabs a:hover {
                border-color: #F60;
                color: #666
            }


        }
        @media (max-width: 767px) {
            .navbar-default .navbar-nav .open .dropdown-menu > li > a {
                color: #081416;
            }

            .navbar-default .navbar-nav .open .dropdown-menu > li > a:hover,
            .navbar-default .navbar-nav .open .dropdown-menu > li > a:focus {
                color: #f6ebeb;
            }

            .navbar-default .navbar-nav .open .dropdown-menu > .active > a,
            .navbar-default .navbar-nav .open .dropdown-menu > .active > a:hover,
            .navbar-default .navbar-nav .open .dropdown-menu > .active > a:focus {
                color: #f6ebeb;
                background-color: #c0392b;
            }
        }
    </style>
@stop
