@extends('officio.main')
@section('title','edit Service Charge')

@section('content')
{{----}}
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Edit Service Charge
    </h1>
</section>
@include('officio.flash.message')

<section class="content">
    {!! Form::model($serviceCharge, [
        'route' => ['service_charge.update', $serviceCharge->id],
        'class' => 'form-horizontal',
        'method'=> 'PUT'
    ])
!!}

    <div class="row">
        <div class="col-md-10">
            @include('officio.servicecharge.form')
        </div>
    </div>

    <div class="text-right border-top">
        <button type="submit" class="btn btn-warning">Update</button>
    </div>



    {!! Form::close() !!}

</section>
@stop


