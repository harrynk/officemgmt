<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    protected $fillable=['name','title','description','percentage','image','email','first_name','last_name'];
}
