<?php

use Illuminate\Database\Seeder;

class UserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userRoles =
            array(
                  array('role_type'=>'Admin'),
                array('role_type'=>'Staff'),
                array('role_type'=>'Customer'),
            );
        \App\UserRole::insert($userRoles);
    }
}
