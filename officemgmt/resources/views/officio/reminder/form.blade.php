<div class="box-body">
 <div class="form-group" {{ $errors->has('subject') ? ' has-error' : '' }}>
    <label for="subject" class="col-sm-2 control-label">Subject:<span class=help-block"
        style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-10">
          {!! Form::text('subject', null , ['class'=> 'form-control', 'placeholder' => ' subject', 'id'=>"subject"]) !!}

          @if ($errors->has('subject'))
          <span class="help-block">
            <strong> {{ $errors->first('subject') }}</strong>
        </span>
        @endif

    </div>
</div>

<div class="form-group" {{ $errors->has('date') ? ' has-error' : '' }}>
    <label for="date" class="col-sm-2 control-label">Date:<span class=help-block"
        style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-10">
            {!! Form::date('date', null , ['class'=> 'form-control', 'placeholder' => ' date', 'id'=>"date"]) !!}

            @if ($errors->has('date'))
            <span class="help-block">
                <strong> {{ $errors->first('date') }}</strong>
            </span>
            @endif

        </div>
    </div>


    <div class="form-group" {{ $errors->has('message') ? ' has-error' : '' }}>
    <label for="message" class="col-sm-2 control-label">message:<span class=help-block"
            style="color: #b30000">&nbsp;* </span></label>

            <div class="col-sm-10">
               <textarea class="form-control ckeditor" name="message"  id="message" >
               </textarea>

               @if ($errors->has('message'))
               <span class="help-block">
               <strong> {{ $errors->first('message') }}</strong>
            </span>
            @endif

        </div>
    </div>

   



            @if(Request::segment(4) == 'edit')
            <div class="form-group">
                <label for="status" class="col-sm-2 control-label">Status<span class=help-block" style="color: #b30000">&nbsp;* </span></label>

                <div class="col-sm-8">

                    <label class="radio-inline"><input type="radio" id="active" name="status" value=1
                     @if($reminder->status==1) checked @endif >Active</label>&nbsp;
                     <label class="radio-inline"><input type="radio" id="inactive" name="status" value=0
                         @if($reminder->status==0) checked @endif >Inactive</label>
                     </div>
                 </div>
                 @endif
             </div>
