@extends('officio.main')
<!-- @section('title','add Dataflow Status') -->

@section('content')
{{----}}

@include('officio.flash.message')<section class="content-header">
<section class="content-header">
    <div class="row">
      <div class="container-fluid">
          <ul id="minitabs">
            <li><a class="current personal-form" href="#">DHA Status</a></li>
            <li><a href="javascript::void" class="address-form">Dataflow Status</a></li>
            <li><a href="javascript::void" class="visa-form" >Exam Status</a></li>

        </ul>
    </div>
</div>
</section>
<section class="content main-case-content">
  <div class="row">

    <div role="form" class="form-horizontal">

       <div class="hide-personal-container">
         
        <div class="row">
            <h3>Add DHA Status</h3>
            <div class="col-md-8">
                {{ Form::open(['url'=>'admin/dhastatus', 'method'=>'POST', 'class' => 'form-horizontal','enctype'=>'multipart/form-data' ]) }}
                  <div class="box-header">
            <!-- <i class="ion ion-clipboard"></i> -->
            <h3 class="box-title"><a href="javascript:void" class="anchor-color"><i class="fa fa-sort-desc form-inline alt-form" aria-hidden="true"></i></a>Add Personal Details</h3>
        </div>
                @include('officio.status.dhaform')

                <div class="text-right border-top">
                    <button type="submit" class="btn btn-warning">Create Profile</button>
                </div>
                {{ form::close() }}
            </div>
        </div>
    </div>

<div class="hide-address-container">
 
<h3>Add Dataflow Status</h3>
<div class="col-md-8">
    {{ Form::open(['url'=>'admin/dataflowform', 'method'=>'POST', 'class' => 'form-horizontal','enctype'=>'multipart/form-data' ]) }}
      <div class="box-header">
    <!-- <i class="ion ion-clipboard"></i> -->

    <h3 class="box-title"><a href="javascript:void" class="anchor-color"><i class="fa fa-sort-desc form-inline alt-form" aria-hidden="true"></i></a>Address/Travel</h3>
</div>
    @include('officio.status.dataflowform')
    <div class="text-right border-top">
        <button type="submit" class="btn btn-warning">ADD</button>
    </div>
    {{ form::close() }}
</div>
</div>

<div class="visa-passport-container">
 
<h3>Add Exam Status</h3>
<div class="col-md-8">
    {{ Form::open(['url'=>'admin/examstatusform', 'method'=>'POST', 'class' => 'form-horizontal','enctype'=>'multipart/form-data' ]) }}
      <div class="box-header">
    <h3 class="box-title"><a href="javascript:void" class="anchor-color"><i class="fa fa-sort-desc form-inline alt-form" aria-hidden="true"></i></a>Passport</h3>
</div>
    @include('officio.status.examstatusform')
    <div class="text-right border-top">
        <button type="submit" class="btn btn-warning">ADD</button>
    </div>
    {{ form::close() }}
</div>
</div>
</div>




</div>
</section>
<!-- <script src="{{URL::to('js/jQuery.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{URL::to('js/jquery.validate.min.js')}}"></script>


<script type="text/javascript">
    function ShowHideDiv(value) {
        var tableins = document.getElementById("eligibility");
        tableins.style.display = value.checked ? "block" : "none";
    }
</script>
<script>
    $('input[type="checkbox"]').on('change', function() {
        $('input[name="' + this.name + '"]').not(this).prop('checked', false);
    });
</script>

<script>
    $(document).ready(function(){
        $('#checkbox1').change(function(){
            if(this.checked)
                $('#eligibility').fadeOut();


            else
                $('#eligibility').fadeIn('slow');

        });
    });
    $(function(){
        $('ul#minitabs li a').click(function(){
            $('ul#minitabs li a').removeClass('current');
            $(this).addClass('current');
        })
    });

</script> -->
<style>
 

  section.content.main-case-content {
    background: green;
    border-radius: 4px;
    margin: 40px;
  }

  .form-horizontal .form-group{
    color:#eee;
  }

  .box-header {
   padding: 20px 0 6px;
   margin: 0 0 15px 0;
 }
 .box-header>.fa, .box-header>.glyphicon, .box-header>.ion, .box-header .box-title {
  padding:0 0 0 -12px;
  color: white;
}
.anchor-color{
  color:white;
}
.anchor-color:hover{
  color:white;
}
.anchor-color:visited a{
  color:white;
}
.fa-sort-down:before, .fa-sort-desc:before {
 color: white;
}
.fa-sort-up:before, .fa-sort-asc:before {
 color: white;
 margin:0;
}

i.fa.form-inline.alt-form {
  border: 1px solid #fff;
  padding: 0px 6px 4px 6px !important;
  border-radius: 5px;
}
</style>
<script type="text/javascript">
    $(function(){
        $('.hide-address-container').hide();
        $('.visa-passport-container').hide();
       

        $('.address-form').click(function(){
        //$('.address-form').attr('class', 'current');
        $('.hide-personal-container').hide();
        $('.visa-passport-container').hide();
     

  });

        $('.visa-form').click(function(){
            $('.hide-personal-container').hide();
            $('.hide-address-container').hide();
       
        });
     


    });
    $(function(){
        $('.box-title a').click(function () {
      $(this).next('a').slideToggle('500');
      $(this).find('i').toggleClass('fa-sort-desc fa-sort-asc')
    });
    });
    


    /*javascript to select active color in nav bar*/
    $(function(){
        $('ul#minitabs li a').click(function(){
            $('ul#minitabs li a').removeClass('current');
      $(this).addClass('current');
    })
    });
</script>
<style type="text/css">

    ul#minitabs {
        list-style: none;
        margin: 0;
        padding: 7px 0;
        border-bottom: 1px solid #CCC;
        font-weight: bold;
        text-align: center;
        white-space: nowrap
    }
    ul#minitabs li {
        display: inline;
        margin: 0 3px;
        padding: 0 40px 0 0;
    }
    ul#minitabs a {
        text-decoration: none;
        padding: 0 0 3px;
        border-bottom: 4px solid #f7f5f5;
        /* color: #999; */
        color: #008000;
    }

    ul#minitabs a.current {
        border-color: #F60;
        /*color:#06F;*/
        color: #337ab7;
    }

    ul#minitabs a:hover {
        border-color: #F60;
        color: #666;
    }

    .my-text {
        font-family: "Courier-new";
        font-size: 22px;
    }

    .my-icon, .alt-form, .pri-form, .case-form, .case-app-form, .address-history-form, .emp-form, .academic-form, .members-form, .old-passport-form, .other-name-form, .pre-aus-travel-form, .pre-other-travel-form, .visa-issue-form, .character-form, .medical-form, .health-form, .language-form, .police-form, .pre-relation-form, .task-form {
        vertical-align: middle;
        font-size: 20px;
    }

    .fa {
        background: none;
        margin: 3px 11px 10px 0 !important;
        border-radius: 1px solid #000 !important;
    }


    @media screen and (max-width: 700px){

        ul#minitabs {
            list-style: none;
            margin: 0;
            padding: 7px 3px;
            border-bottom: 1px solid #CCC;
            font-weight: bold;
            font-size:10px;
            text-align: center;
            white-space: nowrap
        }
        ul#minitabs li {
            display: inline;
            margin: 0 3px;
            padding: 0 6px 0 0;
        }
        ul#minitabs a {
            text-decoration: none;
            padding: 0 0 3px;
            border-bottom: 4px solid #FFF;
            color: #999
        }
        ul#minitabs a.current {
            border-color: #F60;
            color:#06F
        }
        ul#minitabs a:hover {
            border-color: #F60;
            color: #666
        }

        @media screen and (max-width: 480px){

            ul#minitabs {
                list-style: none;
                margin: 0;
                padding: 7px 10px 8px 0;
                border-bottom: 1px solid #CCC;
                font-weight: bold;
                font-size:8px;
                text-align: center;
                white-space: nowrap
            }
            ul#minitabs li {
                display: inline;
                margin: 0 3px;
                padding: 0 4px 0 0;
            }
            ul#minitabs a {
                text-decoration: none;
                padding: 0 0 3px;
                border-bottom: 4px solid #FFF;
                color: #999
            }
            ul#minitabs a.current {
                border-color: #F60;
                color:#06F
            }
            ul#minitabs a:hover {
                border-color: #F60;
                color: #666
            }


        }
        @media (max-width: 767px) {
            .navbar-default .navbar-nav .open .dropdown-menu > li > a {
                color: #081416;
            }

            .navbar-default .navbar-nav .open .dropdown-menu > li > a:hover,
            .navbar-default .navbar-nav .open .dropdown-menu > li > a:focus {
                color: #f6ebeb;
            }

            .navbar-default .navbar-nav .open .dropdown-menu > .active > a,
            .navbar-default .navbar-nav .open .dropdown-menu > .active > a:hover,
            .navbar-default .navbar-nav .open .dropdown-menu > .active > a:focus {
                color: #f6ebeb;
                background-color: #c0392b;
            }
        }
    </style>
    @stop
