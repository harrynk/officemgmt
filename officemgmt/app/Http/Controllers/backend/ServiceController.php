<?php
namespace App\Http\Controllers\backend;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Service;
class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::all();
     
        return view('officio.service.index',compact('services'))->with('title','Griffith service');
    }
    /**Griffith 
     * Show the form for creating a new resource.
     *ResponseC
     * @return \Illuminate\Http\
     */
    public function create()
    {
        return view('officio.service.create')->with('title','Create Griffith service');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
           
            'name' => 'required',
            'description' => 'required',
           
           
        ]);
        $input = $request->all();
        if ($input) {
            Service::create($input);
            session()->flash('message', 'Griffith service Created.');
            return redirect('admin/service');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service = Service::find($id);
        return view('officio.service.edit',compact('service'))->with('title','Edit Griffith service');
    }

    /**
     * Update the specified rescashOutource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
           'description' => 'required',
            'name' => 'required',
          
        ]);
        $service = Service::find($id);
        $input = $request->all();
        $service->update($input);
        session()->flash('message', 'Griffith service Updated.');
        return redirect('admin/service');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!request()->ajax()){
            return false;
        }

        Service::find($id)->delete();


        session()->flash('message', 'Griffith service Deleted.');

        return response()->json(array(
            'status' => 'success',
        ));
    }
}

