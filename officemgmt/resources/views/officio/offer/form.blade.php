<div class="box-body">

    
    <div class="form-group" {{ $errors->has('name') ? ' has-error' : '' }}>
        <label for="name" class="col-sm-2 control-label">name:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">
          {!! Form::text('name', null , ['class'=> 'form-control', 'placeholder' => ' name', 'id'=>"name"]) !!}

            @if ($errors->has('name'))
                <span class="help-block">
                    <strong> {{ $errors->first('name') }}</strong>
                </span>
            @endif

        </div>
    </div>

        <div class="form-group" {{ $errors->has('title') ? ' has-error' : '' }}>
            <label for="title" class="col-sm-2 control-label">title:<span class=help-block"
                style="color: #b30000">&nbsp;* </span></label>

                <div class="col-sm-8">
                    {!! Form::text('title', null , ['class'=> 'form-control', 'placeholder' => ' title', 'id'=>"title"]) !!}

                    @if ($errors->has('title'))
                    <span class="help-block">
                        <strong> {{ $errors->first('title') }}</strong>
                    </span>
                    @endif

                </div>
            </div>
    <div class="form-group" {{ $errors->has('description') ? ' has-error' : '' }}>
        <label for="description" class="col-sm-2 control-label">description:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">
           <textarea class="form-control ckeditor" name="description"  id="description" >
               </textarea>

            @if ($errors->has('description'))
                <span class="help-block">
                    <strong> {{ $errors->first('description') }}</strong>
                </span>
            @endif

        </div>
    </div>
    <div class="form-group" {{ $errors->has('address') ? ' has-error' : '' }}>
        <label for="address" class="col-sm-2 control-label">address:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">
          {!! Form::text('address', null , ['class'=> 'form-control', 'placeholder' => ' address', 'id'=>"address"]) !!}

            @if ($errors->has('address'))
                <span class="help-block">
                    <strong> {{ $errors->first('address') }}</strong>
                </span>
            @endif

        </div>
    </div>
    
    <div class="form-group" {{ $errors->has('percentage') ? ' has-error' : '' }}>
        <label for="percentage" class="col-sm-2 control-label">percentage:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">
          {!! Form::text('percentage', null , ['class'=> 'form-control', 'placeholder' => ' percentage', 'id'=>"percentage"]) !!}

            @if ($errors->has('percentage'))
                <span class="help-block">
                    <strong> {{ $errors->first('percentage') }}</strong>
                </span>
            @endif

        </div>
    </div>
     <div class="form-group" {{ $errors->has('image') ? ' has-error' : '' }}>
        <label for="image" class="col-sm-2 control-label">image:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">
          <!--  <textarea class="form-control ckeditor" name="image"  id="image" >
               </textarea >-->
 {{ Form::textarea('image',null,['class'=>'form-control ckeditor','placeholder'=>'Enter image ']) }}
            @if ($errors->has('image'))
                <span class="help-block">
                    <strong> {{ $errors->first('image') }}</strong>
                </span>
            @endif

        </div>
    </div>
     <div class="form-group" {{ $errors->has('email') ? ' has-error' : '' }}>
        <label for="email" class="col-sm-2 control-label">email:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">
          {!! Form::text('email', null , ['class'=> 'form-control', 'placeholder' => ' email', 'id'=>"email"]) !!}

            @if ($errors->has('email'))
                <span class="help-block">
                    <strong> {{ $errors->first('email') }}</strong>
                </span>
            @endif

        </div>
    </div>
      
    @if(Request::segment(4) == 'edit')
        <div class="form-group">
            <label for="status" class="col-sm-2 control-label">Status<span class=help-block" style="color: #b30000">&nbsp;* </span></label>

            <div class="col-sm-8">

                <label class="radio-inline"><input type="radio" id="active" name="status" value=1
                                                   @if($offer->status==1) checked @endif >Active</label>&nbsp;
                <label class="radio-inline"><input type="radio" id="inactive" name="status" value=0
                                                   @if($offer->status==0) checked @endif >Inactive</label>
            </div>
        </div>
    @endif
</div>
