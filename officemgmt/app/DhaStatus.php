<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class DhaStatus extends Model
{
    protected $fillable=['dha_id','email','username','password'];
    public function dhafees(){
    	return $this->belongsTo('App\DhaFees');
    }

    //  public function user(){
    // 	return $this->belongsTo('App\User');
    // }
}
