@extends('officio.main')
@section('title','add Griffith Reminder')
@section('content')
{{----}}
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Contact information
    </h1>
</section>
@include('officio.flash.message')

<section class="content">
   <div class="row">
    <div class="col-md-12">

        {{--@include('flash.message')--}}
        {{--@include('errors.list')--}}

        <!-- Horizontal Form -->
        <div class="box box-info">
            <!-- form start -->
            <div class="box-body">
                <div class="row">
                  <div class="col-md-8">
                    {{ Form::open(['url'=>'admin/reminder', 'method'=>'POST', 'class' => 'form-horizontal','enctype'=>'multipart/form-data' ]) }}
                    @include('officio.reminder.form')
                    <div class="text-right border-top">
                        <button id="btn" type="submit" class="btn btn-warning">send</button>
                    </div>
                    {{ form::close() }}
                </div>
            </div>
        </div>
    </div><!-- /.box -->


</div>
<script>
$(document).ready(function(){
    $("p").mouseover(function(){
        $("p").css("background-color", "yellow");
    });
   
    $("#btn").click(function(){
        $("p").mouseover();
    });  
    
});
</script>
</section>
@stop
