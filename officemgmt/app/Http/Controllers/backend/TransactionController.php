<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transaction;


class TransactionController extends Controller
{
    /**nder
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transactions = Transaction::all();

        return view('officio.transaction.index',compact('transactions'))->with('title','Transaction');
    }
    /**Griffith 
     * Show the form for creating a new resource.
     *ResponseC
     * @return \Illuminate\Http\
     */
    public function create()
    {
        return view('officio.transaction.create')->with('title','Create Griffith Transaction');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     // dd($request);
        $this->validate($request, [

            'name' => 'required',
            'date' => 'required',
            'amount' => 'required',
            'purpose' => 'required',
            'particular' => 'required',


            ]);
        $input = $request->all();
        if ($input) {
            Transaction::create($input);
            session()->flash('message', 'Griffith Transaction Created.');
            return redirect('admin/transaction');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $transaction = Transaction::find($id);
        return view('officio.transaction.edit',compact('transaction'))->with('title','Edit Griffith Transaction');
    }

    /**
     * Update the specified rescashOutource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
          'name' => 'required',
          'date' => 'required',
          'amount' => 'required',
          'purpose' => 'required',
          // 'particular' => 'required',

          ]);
        $transaction = Transaction::find($id);
        $input = $request->all();
        $transaction->update($input);
        session()->flash('message', 'Griffith Transaction Updated.');
        return redirect('admin/transaction');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!request()->ajax()){
            return false;
        }

        Transaction::find($id)->delete();


        session()->flash('message', 'Griffith Transaction Deleted.');

        return response()->json(array(
            'status' => 'success',
            ));
    }
}

