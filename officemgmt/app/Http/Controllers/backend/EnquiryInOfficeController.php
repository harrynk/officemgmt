<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\EnquiryInOffice;
use DB;

class EnquiryInOfficeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $enquiryinoffices = EnquiryInOffice::all();
     
        return view('officio.enquiryinoffice.index',compact('enquiryinoffices'))->with('title','Enquiry In Office');
    
   }
    public function create()
    {
        return view('officio.enquiryinoffice.create')->with('title','Create Enquiry In Office');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'date' => 'required',
            'name' => 'required',
            'email' => 'required',
            'qualification' => 'required',
            'phone' => 'required',
            'remarks' => 'required',
           
        ]);
        $input = $request->all();
        if ($input) {
           EnquiryInOffice::create($input);
            session()->flash('message', 'Enquiry By Them Created.');
            return redirect('admin/enquiryinoffice'); 
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $enquiryinoffice = EnquiryInOffice::find($id);
        return view('officio.enquiryinoffice.edit',compact('enquiryinoffice'))->with('title','Edit enquiry in office');
    }

    /**
     * Update the specified rescashOutource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
          'date' => 'required',
            'name' => 'required',
            'email' => 'required',
            'qualification' => 'required',
            'phone' => 'required',
            'remarks' => 'required',
        ]);
        $enquiryinoffice = EnquiryInOffice::find($id);
        $input = $request->all();
        $enquiryinoffice->update($input);
        session()->flash('message', 'Enquiry By Them Updated.');
        return redirect('admin/enquiryinoffice');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!request()->ajax()){
            return false;
        }

       EnquiryInOffice::find($id)->delete();


        session()->flash('message', 'Enquiry By Them Deleted.');

        return response()->json(array(
            'status' => 'success',
        ));
    }
}

