@extends('officio.main')
@section('title','edit Griffith Enquiry')

@section('content')
{{----}}
        <!-- Content Header (Pag header) -->
<section class="content-header">
    <h1>
        Edit Griffith Enquiry
    </h1>
</section>
@include('officio.flash.message')

<section class="content">
    {!! Form::model($enquiry, [
        'route' => ['enquiry.update', $enquiry->id],
        'class' => 'form-horizontal',
        'method'=> 'PUT'
    ])
!!}

    <div class="row">
        <div class="col-md-10">
            @include('officio.enquiry.form')
        </div>
    </div>

    <div class="text-right border-top">
        <button type="submit" class="btn btn-warning">Update</button>
    </div>



    {!! Form::close() !!}

</section>
@stop


