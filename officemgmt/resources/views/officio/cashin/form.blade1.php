<div class="box-body">
    <div class="form-group" {{ $errors->has('service_type') ? ' has-error' : '' }}>
        <label for="service_type" class="col-sm-2 control-label">service_type:<span class=help-block"
            style="color: #b30000">&nbsp;* </span></label>

            <div class="col-sm-8">
      <!--       {!! Form::select('service_type', ['D'=>'DHA MCQ/HAAD MCQ','B'=>'Basic Life Support'],null , ['class'=> 'form-control', 'placeholder' => ' service_type', 'id'=>"service_type"]) !!} -->
    <select name="service_type" class="form-control">
                                        <option value="DHA MCQ/HAAD MCQ">DHA MCQ/HAAD MCQ</option>
                                        <option value="Basic Life Support">Basic Life Support</option>
                                    </select>
                @if ($errors->has('service_type'))
                <span class="help-block">
                    <strong> {{ $errors->first('service_type') }}</strong>
                </span>
                @endif

            </div>
        </div>
        <div class="form-group" {{ $errors->has('date') ? ' has-error' : '' }}>
            <label for="date" class="col-sm-2 control-label">date:<span class=help-block"
                style="color: #b30000">&nbsp;* </span></label>

                <div class="col-sm-8">
                    {!! Form::date('date', null , ['class'=> 'form-control', 'placeholder' => ' date', 'id'=>"date"]) !!}

                    @if ($errors->has('date'))
                    <span class="help-block">
                        <strong> {{ $errors->first('date') }}</strong>
                    </span>
                    @endif

                </div>
            </div>

       

        <div class="form-group" {{ $errors->has('service_charge') ? ' has-error' : '' }}>
            <label for="service_charge" class="col-sm-2 control-label">service_charge:<span class=help-block"
                style="color: #b30000">&nbsp;* </span></label>

                <div class="col-sm-8">
                    {!! Form::text('service_charge', null , ['class'=> 'form-control', 'placeholder' => ' service_charge', 'id'=>"service_charge"]) !!}

                    @if ($errors->has('service_charge'))
                    <span class="help-block">
                        <strong> {{ $errors->first('service_charge') }}</strong>
                    </span>
                    @endif

                </div>
            </div>


            <div class="form-group" {{ $errors->has('paid_by') ? ' has-error' : '' }}>
                <label for="paid_by" class="col-sm-2 control-label">paid_by:<span class=help-block"
                    style="color: #b30000">&nbsp;* </span></label>

                    <div class="col-sm-8">
                        {!! Form::text('paid_by', null , ['class'=> 'form-control', 'placeholder' => ' paid_by', 'id'=>"paid_by"]) !!}

                        @if ($errors->has('paid_by'))
                        <span class="help-block">
                            <strong> {{ $errors->first('paid_by') }}</strong>
                        </span>
                        @endif

                    </div>
                </div>
                <div class="form-group" {{ $errors->has('received_by') ? ' has-error' : '' }}>
                    <label for="received_by" class="col-sm-2 control-label">received_by:<span class=help-block"
                        style="color: #b30000">&nbsp;* </span></label>

                        <div class="col-sm-8">
                            {!! Form::text('received_by', null , ['class'=> 'form-control', 'placeholder' => ' received_by', 'id'=>"received_by"]) !!}

                            @if ($errors->has('received_by'))
                            <span class="help-block">
                                <strong> {{ $errors->first('received_by') }}</strong>
                            </span>
                            @endif

                        </div>
                    </div>
                 <!--    <div class="form-group" {{ $errors->has('payment_mode') ? ' has-error' : '' }}>
                        <label for="payment_mode" class="col-sm-2 control-label">payment_mode:<span class=help-block"
                            style="color: #b30000">&nbsp;* </span></label>

                            <div class="col-sm-8">
                                {!! Form::select('payment_mode' ,['c'=>'cash','ch'=>'cheque','b'=>'bank'],null , ['class'=> 'form-control', 'placeholder' => 'payment_mode', 'id'=>"payment_mode"]) !!}

                                @if ($errors->has('payment_mode'))
                                <span class="help-block">
                                    <strong> {{ $errors->first('payment_mode') }}</strong>
                                </span>
                                @endif

                            </div>
                        </div> -->
               <!--      <div class="form-group" {{ $errors->has('date') ? ' has-error' : '' }}>
                        <label for="date" class="col-sm-2 control-label">date:<span class=help-block"
                            style="color: #b30000">&nbsp;* </span></label>

                            <div class="col-sm-8">
                                {!! Form::date('date', null , ['class'=> 'form-control', 'placeholder' => ' date', 'id'=>"date"]) !!}

                                @if ($errors->has('date'))
                                <span class="help-block">
                                    <strong> {{ $errors->first('date') }}</strong>
                                </span>
                                @endif

                            </div>
                        </div> -->
                     <!--    <div class="form-group" {{ $errors->has('nepali_date') ? ' has-error' : '' }}>
                            <label for="nepali_date" class="col-sm-2 control-label">nepali_date:<span class=help-block"
                                style="color: #b30000">&nbsp;* </span></label>

                                <div class="col-sm-8">
                                    {!! Form::date('nepali_date', null , ['class'=> 'form-control', 'placeholder' => ' nepali_date', 'id'=>"nepali_date"]) !!}

                                    @if ($errors->has('nepali_date'))
                                    <span class="help-block">
                                        <strong> {{ $errors->first('nepali_date') }}</strong>
                                    </span>
                                    @endif

                                </div>
                            </div> -->

                         <!--    <div class="form-group" {{ $errors->has('total') ? ' has-error' : '' }}>
                                <label for="total" class="col-sm-2 control-label">Total:<span class=help-block"
                                    style="color: #b30000">&nbsp;* </span></label>

                                    <div class="col-sm-8">
                                        {!! Form::text('total' ,null , ['class'=> 'form-control', 'placeholder' => 'total', 'id'=>"total"]) !!}

                                        @if ($errors->has('total'))
                                        <span class="help-block">
                                            <strong> {{ $errors->first('total') }}</strong>
                                        </span>
                                        @endif

                                    </div>
                                </div> -->
                                @if(Request::segment(4) == 'edit')
                                <div class="form-group">
                                    <label for="status" class="col-sm-2 control-label">Status<span class=help-block" style="color: #b30000">&nbsp;* </span></label>

                                    <div class="col-sm-8">

                                        <label class="radio-inline"><input type="radio" id="active" name="status" value=1
                                           @if($cashIn->status==1) checked @endif >Active</label>&nbsp;
                                           <label class="radio-inline"><input type="radio" id="inactive" name="status" value=0
                                               @if($cashIn->status==0) checked @endif >Inactive</label>
                                           </div>
                                       </div>
                                       @endif
                                   </div>
