@extends('officio.main')
<!-- @section('title','add Dataflow Status') -->

@section('content')
{{----}}

@include('officio.flash.message')
<div class="row">
  <div class="container-fluid">
      <ul id="minitabs">
  
        <li class="active"><a data-toggle="tab" href="#sectionA">DHA Status</a></li>
        <li><a data-toggle="tab" href="#sectionB">Dataflow Status</a></li>
        <li><a data-toggle="tab" href="#sectionC">Exam Status</a></li>
    </ul>
</div>
</div>
<section class="content">
 <div class="row">
    <div class="col-md-12">

        {{--@include('flash.message')--}}
        {{--@include('errors.list')--}}

        <!-- Horizontal Form -->
        <div class="box box-info">
            <!-- form start -->
            <div class="box-body">
                <div class="row">
                  <div id="sectionA" class="tab-pane fade in active">
                   <div class="row">
                    <h3>Add DHA Status</h3>
                    <div class="col-md-8">
                        {{ Form::open(['url'=>'admin/dhastatus', 'method'=>'POST', 'class' => 'form-horizontal','enctype'=>'multipart/form-data' ]) }}
                        @include('officio.status.dhaform')
                        
                        <div class="text-right border-top">
                            <button type="submit" class="btn btn-warning">Create Profile</button>
                        </div>
                        {{ form::close() }}
                    </div>
                </div>
                </div>
                <div id="sectionB" class="tab-pane fade">
                    <h3>Add Dataflow Status</h3>
                    <div class="col-md-8">
                        {{ Form::open(['url'=>'admin/dataflowform', 'method'=>'POST', 'class' => 'form-horizontal','enctype'=>'multipart/form-data' ]) }}
                        @include('officio.status.dataflowform')
                        <div class="text-right border-top">
                            <button type="submit" class="btn btn-warning">ADD</button>
                        </div>
                        {{ form::close() }}
                    </div>
                </div>
        
    <div id="sectionC" class="tab-pane fade">
                    <h3>Add Exam Status</h3>
                    <div class="col-md-8">
                        {{ Form::open(['url'=>'admin/examstatusform', 'method'=>'POST', 'class' => 'form-horizontal','enctype'=>'multipart/form-data' ]) }}
                     <div class="box-body">

    
    <div class="form-group" {{ $errors->has('preferred_date_from') ? ' has-error' : '' }}>
        <label for="preferred_date_from" class="col-sm-2 control-label">Preferred Date From:<span class=help-block"
                                                                    style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">
          {!! Form::date('preferred_date_from', null , ['class'=> 'form-control', 'placeholder' => ' preferred_date_from', 'id'=>"preferred_date_from"]) !!}

            @if ($errors->has('preferred_date_from'))
                <span class="help-block">
                    <strong> {{ $errors->first('preferred_date_from') }}</strong>
                </span>
            @endif

        </div>
    </div>

        <div class="form-group" {{ $errors->has('preferred_date_to') ? ' has-error' : '' }}>
            <label for="preferred_date_to" class="col-sm-2 control-label">TO:<span class=help-block"
                style="color: #b30000">&nbsp;* </span></label>

                <div class="col-sm-8">
                    {!! Form::date('preferred_date_to', null , ['class'=> 'form-control', 'placeholder' => ' preferred_date_to', 'id'=>"preferred_date_to"]) !!}

                    @if ($errors->has('preferred_date_to'))
                    <span class="help-block">
                        <strong> {{ $errors->first('preferred_date_to') }}</strong>
                    </span>
                    @endif

                </div>
            </div>

        <div class="form-group" {{ $errors->has('dha/mcq') ? ' has-error' : '' }}>
            <label for="dha/mcq" class="col-sm-2 control-label">DHA/MCQ :<span class=help-block"
                style="color: #b30000">&nbsp;* </span></label>

                <div class="col-sm-8">
                   <!--  {!! Form::text('dha/mcq', null , ['class'=> 'form-control', 'placeholder' => ' dha/mcq', 'id'=>"dha/mcq"]) !!} -->
  <input type="radio"  name="dha/mcq" value="Pass"/> Pass &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <input type="radio"  name="dha/mcq" value="Fail"/> Fail
                    @if ($errors->has('dha/mcq'))
                    <span class="help-block">
                        <strong> {{ $errors->first('dha/mcq') }}</strong>
                    </span>
                    @endif

                </div>
            </div>


        <div class="form-group" {{ $errors->has('exam_date_booked') ? ' has-error' : '' }}>
            <label for="exam_date_booked" class="col-sm-2 control-label">Exam Date Booked:<span class=help-block"
                style="color: #b30000">&nbsp;* </span></label>

                <div class="col-sm-8">
                    {!! Form::date('exam_date_booked', null , ['class'=> 'form-control', 'placeholder' => ' exam_date_booked', 'id'=>"exam_date_booked"]) !!}

                    @if ($errors->has('exam_date_booked'))
                    <span class="help-block">
                        <strong> {{ $errors->first('exam_date_booked') }}</strong>
                    </span>
                    @endif

                </div>
            </div>

        <div class="form-group" {{ $errors->has('dha_status') ? ' has-error' : '' }}>
            <label for="dha_status" class="col-sm-2 control-label">DHA Status :<span class=help-block"
                style="color: #b30000">&nbsp;* </span></label>

                <div class="col-sm-8">
                 <!--    {!! Form::text('dha_status', null , ['class'=> 'form-control', 'placeholder' => ' dha_status', 'id'=>"dha_status"]) !!} -->
   <input type="checkbox" onclick="ShowHideDiv(this)" id="value"  name="dha_status" value="Pass"/> Pass &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <input type="checkbox"  id="checkbox1" class="dha_status" name="dha_status" value="Fail"/> Fail
                    @if ($errors->has('dha_status'))
                    <span class="help-block">
                        <strong> {{ $errors->first('dha_status') }}</strong>
                    </span>
                    @endif

                </div>
            </div>
      
        <div class="form-group" {{ $errors->has('eligibility') ? ' has-error' : '' }}>
            <label for="eligibility" class="col-sm-2 control-label">Eligibility:<span class=help-block"
                style="color: #b30000">&nbsp;* </span></label>

                <div class="col-sm-8">
                 <!--    {!! Form::text('eligibility', null , ['class'=> 'form-control', 'placeholder' => ' eligibility', 'id'=>"eligibility"]) !!} -->
   <input type="checkbox" id="eligibility" name="eligibility"  value="Yes"/> Yes &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <input type="checkbox" id="eligibility" name="eligibility" value="No"/> No
                    @if ($errors->has('eligibility'))
                    <span class="help-block">
                        <strong> {{ $errors->first('eligibility') }}</strong>
                    </span>
                    @endif

                </div>
            </div>
    @if(Request::segment(4) == 'edit')
        <div class="form-group">
            <label for="status" class="col-sm-2 control-label">Status<span class=help-block" style="color: #b30000">&nbsp;* </span></label>

            <div class="col-sm-8">

                <label class="radio-inline"><input type="radio" id="active" name="status" value=1
                                                   @if($examstatus->status==1) checked @endif >Active</label>&nbsp;
                <label class="radio-inline"><input type="radio" id="inactive" name="status" value=0
                                                   @if($examstatus->status==0) checked @endif >Inactive</label>
            </div>
        </div>
    @endif
</div>

                        <div class="text-right border-top">
                            <button type="submit" class="btn btn-warning">ADD</button>
                        </div>
                        {{ form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /.box -->
    </div>


</div>
</section>
<script src="{{URL::to('js/jQuery.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{URL::to('js/jquery.validate.min.js')}}"></script>


<script type="text/javascript">
    function ShowHideDiv(value) {
        var tableins = document.getElementById("eligibility");
        tableins.style.display = value.checked ? "block" : "none";
    }
</script>
<script>
    $('input[type="checkbox"]').on('change', function() {
        $('input[name="' + this.name + '"]').not(this).prop('checked', false);
    });
</script>

<script>
    $(document).ready(function(){
        $('#checkbox1').change(function(){
            if(this.checked)
                $('#eligibility').fadeOut();


            else
                $('#eligibility').fadeIn('slow');

        });
    });
        $(function(){
        $('ul#minitabs li a').click(function(){
            $('ul#minitabs li a').removeClass('current');
      $(this).addClass('current');
    })
    });
        
</script>
<style type="text/css">

    ul#minitabs {
        list-style: none;
        margin: 0;
        padding: 7px 0;
        border-bottom: 1px solid #CCC;
        font-weight: bold;
        text-align: center;
        white-space: nowrap
    }
    ul#minitabs li {
        display: inline;
        margin: 0 3px;
        padding: 0 40px 0 0;
    }
    ul#minitabs a {
        text-decoration: none;
        padding: 0 0 3px;
        border-bottom: 4px solid #f7f5f5;
        /* color: #999; */
        color: #008000;
    }

    ul#minitabs a.current {
        border-color: #F60;
        /*color:#06F;*/
        color: #337ab7;
    }

    ul#minitabs a:hover {
        border-color: #F60;
        color: #666;
    }

    .my-text {
        font-family: "Courier-new";
        font-size: 22px;
    }

    .my-icon, .alt-form, .pri-form, .case-form, .case-app-form, .address-history-form, .emp-form, .academic-form, .members-form, .old-passport-form, .other-name-form, .pre-aus-travel-form, .pre-other-travel-form, .visa-issue-form, .character-form, .medical-form, .health-form, .language-form, .police-form, .pre-relation-form, .task-form {
        vertical-align: middle;
        font-size: 20px;
    }

    .fa {
        background: none;
        margin: 3px 11px 10px 0 !important;
        border-radius: 1px solid #000 !important;
    }


    @media screen and (max-width: 700px){

        ul#minitabs {
            list-style: none;
            margin: 0;
            padding: 7px 3px;
            border-bottom: 1px solid #CCC;
            font-weight: bold;
            font-size:10px;
            text-align: center;
            white-space: nowrap
        }
        ul#minitabs li {
            display: inline;
            margin: 0 3px;
            padding: 0 6px 0 0;
        }
        ul#minitabs a {
            text-decoration: none;
            padding: 0 0 3px;
            border-bottom: 4px solid #FFF;
            color: #999
        }
        ul#minitabs a.current {
            border-color: #F60;
            color:#06F
        }
        ul#minitabs a:hover {
            border-color: #F60;
            color: #666
        }

        @media screen and (max-width: 480px){

            ul#minitabs {
                list-style: none;
                margin: 0;
                padding: 7px 10px 8px 0;
                border-bottom: 1px solid #CCC;
                font-weight: bold;
                font-size:8px;
                text-align: center;
                white-space: nowrap
            }
            ul#minitabs li {
                display: inline;
                margin: 0 3px;
                padding: 0 4px 0 0;
            }
            ul#minitabs a {
                text-decoration: none;
                padding: 0 0 3px;
                border-bottom: 4px solid #FFF;
                color: #999
            }
            ul#minitabs a.current {
                border-color: #F60;
                color:#06F
            }
            ul#minitabs a:hover {
                border-color: #F60;
                color: #666
            }


        }
        @media (max-width: 767px) {
            .navbar-default .navbar-nav .open .dropdown-menu > li > a {
                color: #081416;
            }

            .navbar-default .navbar-nav .open .dropdown-menu > li > a:hover,
            .navbar-default .navbar-nav .open .dropdown-menu > li > a:focus {
                color: #f6ebeb;
            }

            .navbar-default .navbar-nav .open .dropdown-menu > .active > a,
            .navbar-default .navbar-nav .open .dropdown-menu > .active > a:hover,
            .navbar-default .navbar-nav .open .dropdown-menu > .active > a:focus {
                color: #f6ebeb;
                background-color: #c0392b;
            }
        }
    </style>
    @stop
