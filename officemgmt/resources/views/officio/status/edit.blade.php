@extends('officio.main')
@section('title','edit Cash Out')

@section('content')
{{----}}
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Edit Cash Out
    </h1>
</section>
@include('officio.flash.message')

<section class="content">
    {!! Form::model($cashOut, [
        'route' => ['cash_out.update', $cashOut->id],
        'class' => 'form-horizontal',
        'method'=> 'PUT'
    ])
!!}

    <div class="row">
        <div class="col-md-10">
            @include('officio.cashout.form')
        </div>
    </div>

    <div class="text-right border-top">
        <button type="submit" class="btn btn-warning">Update</button>
    </div>



    {!! Form::close() !!}

</section>
@stop


