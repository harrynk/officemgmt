@extends('officio.main')
@section('title','DHA/Fees')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>

       <span>

          View DHA/Invoice

      </span>
  </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">

            @include('officio.flash.message')

            <div class="box">
                <div class="box-body">
                    <div class="table-reponsive">
                        <table id="example1" class="table table-bordered table-striped user-list">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Generated date</th>

                                </tr>
                            </thead>
                            <tbody>

                                <?php
                                $i=1;
                                ?>


                                @foreach ($invoices as $value) {
                                <?php
                                    $servicecharge = \App\ServiceCharge::where('id',$value->service_charge_id)->first();
                                   
                                    ?>
                                    <tr>
                                        <td> {{ $servicecharge->name }}</td>
                                        <td>{{ date('F d, Y', strtotime($servicecharge->created_at)) }}</td>

                                    
                                </tr>  
                                } 
                                @endforeach
                                
                            </tbody>    
                        </table>
                    </div>  
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->

    </div><!-- /.row -->
</section><!-- /.content -->

<script>
    $(function () {
        $('#example1').DataTable({
            "pageLength": 100,
            "dom": '<"top"pfl<"clear">>rt<"bottom"p<"clear">>'
        });
    });
</script>

@stop