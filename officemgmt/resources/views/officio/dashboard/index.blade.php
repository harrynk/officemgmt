@extends('officio.main')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
     Show Records
    </h1>

</section>
   
  <div>    
{{ Form::open(['url'=>'admin/report', 'method'=>'POST', 'class' => 'form-horizontal','enctype'=>'multipart/form-data' ]) }}          

            <div class="col-sm-4">             
                <select name="month" class="form-control">
                <option value="0
                    ">Select Month
                </option>
                    <option value="1
                    ">Jan
                </option>
                <option value="2
                    ">Feb
                </option><option value="3
                    ">Mar
                </option><option value="4
                    ">Apr
                </option><option value="5
                    ">May
                </option><option value="6
                    ">Jun
                </option><option value="7
                    ">Jul
                </option><option value="8
                    ">Aug
                </option><option value="9
                    ">Sept
                </option><option value="10
                    ">Oct
                </option><option value="11
                    ">Nov
                </option><option value="12
                    ">Dec
                </option>
                
                
            </select>
            <button type="submit" class="btn btn-primary"> Show Report</button>
            

        </div>
        {{form::close()}}
    </div>
    <div>

{{ Form::open(['url'=>'admin/nepalicalender', 'method'=>'POST', 'class' => 'form-horizontal','enctype'=>'multipart/form-data' ]) }}          

            <div class="col-sm-4">             
                <select name="month" class="form-control">
                <option value="0
                    ">Select Month
                </option>
                    <option value="1
                    ">Baisakh
                </option>
                <option value="2
                    ">Jestha
                </option><option value="3
                    ">Ashad
                </option><option value="4
                    ">Shrawan
                </option><option value="5
                    ">Bhadra
                </option><option value="6
                    ">Ashoj
                </option><option value="7
                    ">Kartik
                </option><option value="8
                    ">Mangsir
                </option><option value="9
                    ">Poush
                </option><option value="10
                    ">Magh
                </option><option value="11
                    ">Falgun
                </option><option value="12
                    ">Chaitra
                </option>
                
                
            </select>
            <button type="submit" class="btn btn-primary"> Show Report</button>
            

        </div>
        {{form::close()}}
   </div>
@stop

