<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceCharge extends Model
{
  protected $fillable=['id','name','qualification','applied_for','service_charge','total'];
    public function Report()
   {
   	return $this->hasMany('App\Report');
   }
   public function invoice(){
   	return $this->hasMany('App\invoice');
   }
}
