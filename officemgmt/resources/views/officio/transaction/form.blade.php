<div class="box-body">

    
    <div class="form-group" {{ $errors->has('name') ? ' has-error' : '' }}>
        <label for="name" class="col-sm-2 control-label">name:<span class=help-block"
            style="color: #b30000">&nbsp;* </span></label>

            <div class="col-sm-8">
              {!! Form::text('name', null , ['class'=> 'form-control', 'placeholder' => ' name', 'id'=>"name"]) !!}

              @if ($errors->has('name'))
              <span class="help-block">
                <strong> {{ $errors->first('name') }}</strong>
            </span>
            @endif

        </div>
    </div>

    <div class="form-group" {{ $errors->has('date') ? ' has-error' : '' }}>
        <label for="date" class="col-sm-2 control-label">date:<span class=help-block"
            style="color: #b30000">&nbsp;* </span></label>

            <div class="col-sm-8">
                {!! Form::date('date', null , ['class'=> 'form-control', 'placeholder' => ' date', 'id'=>"date"]) !!}

                @if ($errors->has('date'))
                <span class="help-block">
                    <strong> {{ $errors->first('date') }}</strong>
                </span>
                @endif

            </div>
        </div>
        <div class="form-group" {{ $errors->has('amount') ? ' has-error' : '' }}>
            <label for="amount" class="col-sm-2 control-label">amount:<span class=help-block"
                style="color: #b30000">&nbsp;* </span></label>

                <div class="col-sm-8">
                  {!! Form::text('amount', null , ['class'=> 'form-control', 'placeholder' => ' amount', 'id'=>"amount"]) !!}

                  @if ($errors->has('amount'))
                  <span class="help-block">
                    <strong> {{ $errors->first('amount') }}</strong>
                </span>
                @endif

            </div>
        </div>
        <div class="form-group" {{ $errors->has('purpose') ? ' has-error' : '' }}>
            <label for="purpose" class="col-sm-2 control-label">purpose:<span class=help-block"
                style="color: #b30000">&nbsp;* </span></label>

                <div class="col-sm-8">
                  {!! Form::text('purpose', null , ['class'=> 'form-control', 'placeholder' => ' purpose', 'id'=>"purpose"]) !!}

                  @if ($errors->has('purpose'))
                  <span class="help-block">
                    <strong> {{ $errors->first('purpose') }}</strong>
                </span>
                @endif

            </div>
        </div>
  
      <div class="form-group" {{ $errors->has('particular') ? ' has-error' : '' }}>
            <label for="particular" class="col-sm-2 control-label">particular:<span class=help-block"
              style="color: #b30000">&nbsp;* </span></label>

              <div class="col-sm-8">
                 <!--   {!! Form::text('particular', null , ['class'=> 'form-control', 'placeholder' => ' particular', 'id'=>"particular"]) !!} -->
                <select name="particular" class="form-control">
                 <option value="Cheaque In">Cheaque In</option>
                   <option value="Cheaque Out">Cheaque Out</option>
                   <option value="Deposit Slip In">Deposit Slip In</option>
                   <option value="Deposit Slip In">Deposit Slip Out</option>
                   <option value="Griffith UAE In">Griffith UAE In</option>
                   <option value="Griffith UAE Out">Griffith UAE Out</option>
                </select>
                @if ($errors->has('particular'))
                <span class="help-block">
                  <strong> {{ $errors->first('particular') }}</strong>
                </span>
                
                @endif

              </div>
            </div>
    @if(Request::segment(4) == 'edit')
    <div class="form-group">
        <label for="status" class="col-sm-2 control-label">Status<span class=help-block" style="color: #b30000">&nbsp;* </span></label>

        <div class="col-sm-8">

            <label class="radio-inline"><input type="radio" id="active" name="status" value=1
               @if($transaction->status==1) checked @endif >Active</label>&nbsp;
               <label class="radio-inline"><input type="radio" id="inactive" name="status" value=0
                   @if($transaction->status==0) checked @endif >Inactive</label>
               </div>
           </div>
           @endif
       </div>
